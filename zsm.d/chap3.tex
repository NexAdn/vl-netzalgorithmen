\chapter{Optimization Methods}

\section{Optimization Problems}

An \emph{optimization problem}\index{optimization problem} is given by
a set \(M\) and a function \(f: X \to \mathbb{R}\) with \(M \subseteq X\).
The usual terminology is as follows:
\begin{itemize}
	\item \(M\) is called the \emph{feasible set}\index{feasible set}
	\item \(f\) is called the \emph{objective function}\index{objective function}
	\item Elements of \(M\) are called \emph{feasible solutions}\index{feasible solution}
	\item \(x^{\ast} \in M\) is an \emph{optimal solution}\index{optimal solution}
		if \(\forall x \in M: f\left(x^{\ast}\right) \geq f\left(x\right)\), i.\,e.
		\begin{align}
			f\left(x^{\ast}\right) &= \max\left\{f\left(x\right) \mmid x \in M \right\}
		\end{align}
\end{itemize}

An \emph{optimization method}\index{optimization method} is an algorithm
that computes an optimal solution \(x^{\ast}\) given the input \(\left(M, f\right)\),
if there is any.

There is no need to deal with minimaztion problems separately as
they can be easily converted into a maximization problem.

\section[Linear Optimization Problems]{Linear Optimization Problems (LOP)}

An optimization problem \(\left(M, f\right)\) is a
\emph{linear optimization problem}\index{linear optimization problem} (LOP)\index{LOP}
if \(M \subseteq \mathbb{R}^n\) for some \(n \in \mathbb{N}\) consists of all
\(\vec{x} \in \mathbb{R}^n\) satisfying a finite set of linear inequalitites
and/or linear equations, and \(f: \mathbb{R}^n \to \mathbb{R}\) is a linear function.

Example:

\begin{align}
	\max f\left(x, y\right) &= x + 3y\\
	\shortintertext{subject to}
	-x + y &\leq 1\\
	x + y &\leq 2\\
	x, y &\geq 0
\end{align}

Notation:
\begin{itemize}
	\item \(\mathbb{R}^n\) is the set of all column vectors
		\(\vec{x} = \left(x_1, \dotsc, x_n\right)^{\top}\)
		with \(x_1, \dotsc, x_n \in \mathbb{R}\)
	\item \(\vec{a} \leq \vec{b}\) iff \(a_k < b_k\) for all
		\(k \in \left\{1, \dotsc, n\right\}\)
	\item \(\vec{o}\) denotes a zero vector
	\item \(I\) denotes an identity matrix
	\item \(\vnorm{\vec{x}} = \sqrt{x_1^2 + \dotsc + x_n^2}\) denotes
		the euclidian norm of \(\vec{x}\)
\end{itemize}

\subsection{Solving LOPs with Two Variables Graphically}

Each inequality defines a boundary line, which can be plotted in a diagram.
These together are boundaries to the feasible set.
By setting \(c = f\left(\vec{x}\right)\) for some \(c\),
a contour line can be drawn, showing where \(f\left(\vec{x}\right) = c\) holds.
By increasing \(c\), this contour line is shifted towards the maximum value,
until it reaches a “corner” with the maximum value.

\subsection{Canonical form LOPs}\index{linear optimization problem!canonical form}

\begin{align}
	\max f\left(\vec{x}\right) &= \vec{c}^{\top}\vec{x}\\
	\shortintertext{subject to}
	A\vec{x} &\leq \vec{b}\\
	\vec{x} &\geq \vec{o}
\end{align}
With \(A \in \mathbb{R}^{m\times n}\) being a real matrix with \(m\) rows and \(n\) columns
and \(\vec{b} \in \mathbb{R}^m\) being a real column vector with \(m\) entries.

To transform any LOP into a LOP in canonical form:
\begin{itemize}
	\item If there is a variable \(x_k\) subject to \(x_k \leq 0\)
		(the variable must be non-positive),
		replace every appearance of \(x_k\) with \(-x_k\)
		and replace \(x_k \leq 0\) with \(x_k \geq 0\).
	\item If a variable \(x_k\) is unrestricted (can be both positive and negative),
		replace every appearance of \(x_k\) by \(x_k^+ - x_k^-\)
		and let \(x_k^+ \geq 0\) and \(x_k^- \geq 0\).
	\item Replace \(\underline{a}_k \vec{x} \leq b_k\) with
		\(-\underline{a}_k \vec{x} \geq -b_k\) and \(\underline{a}_k\vec{x} = b_k\)
		with \(\underline{a}_k\vec{x} \leq b_k\) and \(\underline{a}_k\vec{x} \geq b_k\).
\end{itemize}

\subsection{Standard Form LOPs}\index{linear optimization problem!standard form}

\begin{align}
	\max f\left(\vec{x}\right) &= \vec{c}^{\top} \vec{x}\\
	\shortintertext{subject to}
	A\vec{x} &= \vec{b}\\
	\vec{x} &\geq \vec{o}
\end{align}

With \(A \in \mathbb{R}^{m \times n}\) and \(\vec{b} \in \mathbb{R}^m\)
with \(\vec{b} \geq \vec{o}\).

To transform a LOP into standard form,
every occurence of \(\leq\) must be replaced with \(=\)
as well as the introduction of \emph{slack variables}\index{slack variable} \(\vec{y}\),
which are simply added to the left-hand side of the equation
to allow the actual \(x_k\) to be lower than \(b_k\).
The resulting equations can be rewritten as
\begin{align}
	\left(A \middle| I\right) \cvec{\vec{x}\\\vec{y}} &= \vec{b}
\end{align}

If \(\vec{b}\) does not satisfy \(\vec{b} \geq \vec{o}\)
(i.\,e. if some \(b_k\) are negative),
multiply these rows by \(-1\).

LOPs in standard form are systems of linear equations \(A\vec{x} = \vec{b}\).
As such, the number of irredundant equations can be compared to the number of variables
to get information about how many solutions exist:
\begin{itemize}
	\item If there are more irredundant equations than variables,
		no solution to the system of equation exists.
	\item If there are as many irredundant equations as variables,
		there is exactly one solution.
	\item If there are less irredundant equations as variables,
		there are infinitely many solutions
		(and as such, an optimal solution must be determined).
\end{itemize}

\section{The Structure of the Feasible Set}

A subset \(X \subseteq \mathbb{R}^n\) is called:
\begin{description}
	\item[convex]\index{convex} if for any two points \(\vec{x}_1, \vec{x}_2 \in X\)
		the whole straight line segment is in \(X\),
		i.\,e. the straight line connecting \(\vec{x}_1\) and \(\vec{x}_2\)
		never leaves \(X\)
	\item[closed]\index{closed} if the limit of every convergent sequence in \(X\)
		is in \(X\) too
	\item[bounded]\index{bounded} if there is \(K \in \mathbb{R}\) such that
		\(\vnorm{\vec{x}} \leq K\) for all \(\vec{x} \in X\),
		i.\,e.~there is an upper limit to the distance of all points in \(X\) from \(\vec{o}\).
\end{description}

Any feasible set \(M\) for a LOP in standard form is convex and closed.
If \(M\) is also nonempty and bounded, \(f\) attains its maximum on \(M\).

\section{Extreme Points and Basic Solutions}

Let \(M\) be a convex set.
A point \(\vec{x}_0 \in M\) is called an \emph{extreme point}\index{extreme point}
of \(M\) if \(\vec{x}_0\) cannot be expressed as a convex linear combination
\(\alpha \vec{x}_1 + \left(1 - \alpha\right) \vec{x}_2\) with \(\alpha \in \left[0,1\right]\)
of two points \(\vec{x}_1, \vec{x}_2 \in M\) with \(\vec{x}_1 \neq \vec{x}_0\)
and \(\vec{x}_2 \neq \vec{x}_0\).

If the set \(M\) is nonempty, then it has at most finitely many
and at least one extreme point.
If \(M\) is in addition bounded,
then \(M\) is the convex hull of its extreme points
\(\vec{x}_1, \dotsc, \vec{x}_k\), i.\,e.~any point \(\vec{x} \in M\)
can be expressed as convex linear combination of the extreme points.

If \(M\) is nonempty and bounded, then there is an extreme point
\(\vec{x}^{\ast} \in M\) such that
\(f\left(\vec{x}^{\ast}\right) \geq f\left(\vec{x}\right) \quad\forall \vec{x} \in M\).
Thus, one of the extreme points is the optimal solution to the LOP.

If \(M\) is nonempty and unbounded,
then there need not be an optimal solution.
However, if there is one, then the following theorem holds:

If \(M\) is nonempty and there exists a \(K\) such that
\(f\left(\vec{x}\right) \leq K\) for all \(\vec{x} \in M\),
then there is an extreme point \(\vec{x}^{\ast} \in M\)
such that \(f\left(\vec{x}^{\ast}\right) \geq f\left(\vec{x}\right)\) for all \(\vec{x} \in M\).

Let \(A \in \mathbb{R}^{m \times n}\) be a matrix with column vectors
\(\vec{a}_1, \dotsc, \vec{a}_n\),
\(M = \left\{\vec{x} \in \mathbb{R}^n \mmid \left(A\vec{x} = \vec{b}\right) \land \left(\vec{x} \geq \vec{o}\right)\right\}\) and
\(S\left(\vec{x}\right) = \left\{\vec{a}_i \mmid \left(i \in \left\{1, \dotsc, n\right\}\right)
\land \left(x_i \neq 0\right)\right\}\) for \(\vec{x} \in M\).
Then \(\vec{x}\) is called a \emph{basic solution}\index{basic solution}
if \(S\left(\vec{x}\right)\) is linear independent.

\(\vec{x} \in M\) is an extreme point of \(M\) iff \(\vec{x}\) is a basic solution.

\section{The Simplex Algorithm}
\subsection{Overview}

Consider the LOP in standard form (L) as follows:
\begin{align}
	\max f\left(\vec{x}\right) &= \vec{c}^{\top} \vec{x}\\
	\shortintertext{subject to}
	A\vec{x} &= \vec{b}\\
	\vec{x} &\geq \vec{o}\\
	A &\in \mathbb{R}^{m \times n}\\
	\vec{b} &\geq \vec{o}\\
	rk\left(A\right) &= m
\end{align}

A few facts are already known:
\begin{itemize}
	\item \(M\) has at least one extreme point
	\item Any extreme point of \(M\) is a basic solution of (L) and vice versa
	\item (L) has at most \(\cvec{n\\m}\) basic solutions
	\item If there is an optimal solution of (L),
		then there is an optimal solution of (L) that is a basic solution.
	\item If \(M\) is nonempty and bounded, then there is an optimal solution of (L).
\end{itemize}

Thus, if \(M\) is nonempty and bounded, there is a finite algorithm
that finds an optimal (basic) solution of (L).

One such algorithm is the Simplex Algorithm.
It consists of two parts, called Phase 1 and Phase 2.
The input of Phase 2 is a feasible basic solution.
The algorithm stops when either an optimal basic solution has been found or
if it has been detected that the objective function is unbounded on \(M\).
In the latter case, there is no optimal solution of (L).

If no feasible basic solution, Phase 1 must be executed,
which applies Phase 2 to an auxiliary LOP.
Phase 1 stops when a feasible basic solution of (L) Has been found
or if it has been detected that \(M\) is empty.

\subsection{Phase 2}

Let \(\vec{x}\) be a feasible basic solution of (L).
Letz \(T\left(\vec{x}\right) \subseteq\left\{\vec{a}_1, \dotsc, \vec{a}_n\right\}\)
be a maximal linear independent set of column vectors of \(A\)
such that \(x_k \neq 0 \implies \vec{a}_k \in T\left(\vec{x}\right)\),
i.\,e.~\(T\) gives the set of columns of \(A\)
where the basic solution's value is not \(0\).
It is called a \emph{basis}\index{basis} of the linear subspace
generated by all column vectors of \(A\).
Thus, each column vector \(\vec{a}_j\) can be represented
as a linear combination of the elements of \(T\left(\vec{x}\right)\):
\begin{align}
	\vec{a}_j &= \sum_{k \in B}t_{k,j} \vec{a}_k
\end{align}

Let
\(B = \left\{k \in \left\{1, \dotsc, n\right\} \mmid \vec{a}_k \in T\left(\vec{x}\right)\right\}\)
and \(N = \left\{1, \dotsc, n\right\} \setminus B\),
i.\,e.~\(B\) is the set of column indices where there is corresponding \(\vec{a}_k\)
to the basic solution
and \(N\) is the remainder of column indices.

If \(j \in B\), then \(t_{j,j} = 1\) and \(t_{k,j} = 0\) if \(k \neq j\).
For \(j \in N\) let \(u_j = \sum_{k \in B} t_{k,j}c_k\) and \(d_j = u_j - c_j\).
These values can be used to transform \(f\left(\vec{x}\right)\) into \(f\left(\vec{y}\right)\)
for an arbitrary feasible solution \(\vec{y}\).

Three cases can now be distinguished:
\begin{itemize}
	\item All offset factors \(d_i\) for \(i \in N\) are positive.
		Then \(f\left(\vec{y}\right) \leq f\left(\vec{x}\right)\),
		i.\,e.~the basic solution is better than or equal to \(f\left(\vec{y}\right)\)
		for all feasible solutions,
		i.\,e.~the feasible basic solution \(\vec{x}\) is also optimal.
		The algorithm stops.

	\item For some \(j \in N\), all \(d_j < 0\) and \(t_{k,j} \leq 0\) for all \(k \in B\).
		Then the objective function \(f\) is unbounded from above on \(M\),
		i.\,e.~the objective function can get arbitrarily high.
		The algorithm stops.

	\item For some \(j \in N\) with \(d_j < 0\) there is an index \(l \in B\)
		such that \(t_{l,j} > 0\).
		Then a new feasible basic solution \(\vec{z}\) is calculated and the algorithm starts over.
\end{itemize}

Special care is neeeded to avoid “cycling”;
this is done by applying \emph{Bland's rule}\index{Blands rule@Bland's rule},
i.\,e.~always take the lowest such \(j\) and the lowest such basis index
to find a new \(\vec{z}\).

\subsection{Phase 1}

If the original LOP is given in standard form (L),
and no feasible basic solution is known,
an auxiliary LOP can be used to find one:
\begin{align}
	\min g\left(\vec{x}, \vec{y}\right) &= \sum_{i=1}^{m} y_i\\
	\shortintertext{subject to}
	A\vec{x} + \vec{y} &= \vec{b}\\
	\vec{x} &\geq \vec{o}\\
	\vec{y} &\geq \vec{o}
\end{align}

A feasible basic solution for the auxiliary problem is \(\vec{y} = \vec{b}, \vec{x} = \vec{o}\).
Thus, the feasible set of the auxiliary LOP is nonempty.

The auxiliary LOP has an optimal solution \(\left(\vec{y}^{\ast}, \vec{x}^{\ast}\right)\)
and the original LOP has a feasible solution iff \(\vec{y}^{\ast} = \vec{o}\).
Thus, the objective is to find an optimal solution with \(\vec{y} = \vec{o}\).

An optimal solution can be found by applying Phase 2 to the auxiliary LOP.
A basic feasible solution for the auxiliary LOP is
\(\vec{y} = \vec{b}\), \(\vec{x} = \vec{o}\).

\subsection{Simplex Tableaus}

The plain algorithm description can be applied to a tabular view of the system of equations.
The system of linear equations representing \(A\vec{x} = \vec{b}\) can be transformed
by the Gauss-Jordan transformation into equivalent systems of equations.
The following operations can be performed:
\begin{itemize}
	\item Switch two equations (rows)
	\item Multiply one equation (row) with a \emph{nonzero} factor
	\item Add a multiple of an equation (row) to another equation (row)
\end{itemize}
The resulting system of equations has exactly the same solutions as the original one.

With these operations, the system of linear equations \(A\vec{x} = \vec{b}\)
can be transformed into an equivalent system of linear equations
\(A\vec{x} = \vec{b}\) such that the first \(m\) columns form an \(m \times m\)
identity matrix \(I\).

In simplex tableaus, we extend the system of equations \(\hat{A}\vec{x} = \vec{\hat{b}}\)
by adding another row \(\left(\vec{\hat{c}}^{\top}\middle|z\right)\)
with \(\hat{z} = \vec{\hat{c}}^{\top}\vec{x}\) being
the transformed objective function value
and
\begin{align}
	\hat{c}_i &= \begin{cases}
		0 & i \in B\\
		c_i - \sum_{k\in B}c_k t_{k,j} & i \in N
	\end{cases}\\
	\hat{z} &= z - \sum_{k \in B}c_kb_k
\end{align}

This allows us to produce new basic solutions as follows:
\begin{itemize}
	\item Choose \(j \in N\) such that \(d_j < 0\).
	\item Choose \(l \in B\) such that \(t_{l,j} > 0\) and \(\frac{x_l}{t_{l,j}}\) is minimal.
	\item If multiple such \(j\) and \(l\) exist, apply Bland's rule.
	\item Multiply the \(l\)th equation by \(\frac{1}{t_{l,j}}\) and then,
		add the resulting \(l\)th equation multiplied by \(-t_{k,j}\) to the \(k\)th equation
		for \(k \in \left\{1, \dotsc, m\right\}\) with \(k \neq l\).
	\item Add the \(l\)th equation multiplied by \(-\hat{c}_j = -d_j\)
		to the equation \(\vec{c}^{\top}\vec{x} = z\),
		giving \(\vec{\hat{c}}^{\top}\vec{x} = \hat{z}\).
\end{itemize}

This translates into the following steps:
\begin{itemize}
	\item Find the column \(j\) with the highest \(c_j\) (= pivot column).
	\item Divide the values of the last column by the corresponding values from the pivot column.
		Find the row where the result is the lowest (= pivot row).
	\item The intersection between the pivot column and pivot row is the pivot element.
	\item Use Gauss-Jordan transformations to bring all cells of the pivot column
		except the pivot element and the last row to 0.
		While doing so, make sure that \(\vec{b}\) remains positive!
	\item Repeat until:
		\begin{itemize}
			\item All \(c_j\) are \(\leq 0\). An optimal solution has been found. Stop.
			\item For some \(j\) with \(c_j > 0\) all cells \(a_{ij}\) above it are \(a_{ij} \leq 0\).
				Then the solution is unbounded.
		\end{itemize}
\end{itemize}

\subsection{The Complexity of the Simplex Algorithm}

While there are examples where the Simplex Algorithm starting with
a specified feasible basic solution will pass through all vertices,
and the number of vertices is exponential in the number \(n\) of variables.
Still, in practice the Simplex Algorithm is very efficient in approach.
In many relevant cases, its running time is proportional to \(m + n\).

Polynomail time algorithms for LOPs also exist (Khachian, Karmarkar).
Furthermore, there are also polynomial time variations of the Simplex Algorithm
for special cases like single-commodity network flow problems.

\section{Duality}

To every \emph{primal}\index{primal} problem (i.\,e.~any problem)
\begin{align}
	&\max \vec{c}^{\top} \vec{x} &\text{s.\,t. } &A\vec{x} \leq \vec{b}, \vec{x} \geq \vec{o}\\
	\shortintertext{there is a \emph{dual}\index{dual} problem}
	&\min \vec{b}^{\top} \vec{y} &\text{s.\,t. } &A^{\top}\vec{y} \geq \vec{c}, \vec{y} \geq \vec{o}\text{.}
\end{align}
Thus, in the dual problem,
the number of variables and constraints are switched with each other.
If the primal problem had 2 variables and 3 constraints,
the dual problem has 3 variables and 2 constraints.
The optimal solution for both problems is the same and
any feasible solution is a bound for the optimal solution.
If the primal optimal solution is unbounded,
then the dual problem has no solution.
Vice versa, if the primal problem has no solution, the dual problem's optimal solution
is unbounded.

If there are feasible solutions of the primal problem and its objective function
\(\vec{c}^{\top}\vec{x}\) is bounded from above,
then both the primal and dual problem have optimal feasible solutions
\(\vec{x}^{\ast}\) and \(\vec{y}^{\ast}\) and
\begin{align}
	\vec{c}^{\top}\vec{x}^{\ast} &= \left(\vec{y}^{\ast}\right)^{\top} \vec{b}\text{.}
\end{align}
If there are feasible solutions of the dual problem and
its objective function \(\vec{y}^{\top}\vec{b}\) is bounded from below,
both the primal and dual problem have optimal feasible solutions
\(\vec{x}^{\ast}\) and \(\vec{y}^{\ast}\) and
\begin{align}
	\vec{c}^{\top}\vec{x}^{\ast} &= \left(\vec{y}^{\ast}\right)^{\top} \vec{b}\text{.}
\end{align}

The primal problem can be transformed into the dual problem
as shown in \autoref{tab:primal-dual-transform}.

\begin{table}[!htp]
	\centering
	\caption{Transformation between the primal and dual LOP}
	\label{tab:primal-dual-transform}
	\begin{tabulary}{\columnwidth}{Lcc}
		\toprule
		& Primal LOP & Dual LOP\\
		\midrule
		Variables & \(x_1, \dotsc, x_n\) & \(y_1, \dotsc, y_m\)\\
		Matrix & \(A\) & \(A^{\top}\)\\
		Right-hand side & \(\vec{b}\) & \(\vec{c}\)\\
		Objective function & \(\max \vec{c}^{\top}\vec{x}\) & \(\min \vec{b}^{\top}\vec{y}\)\\
		Constraints & \(i\)th constraint has \(\leq\) & \(y_i \geq 0\)\\
		& \(i\)th constraint has \(\geq\) & \(y_i \leq 0\)\\
		& \(i\)th constraint has \(=\) & \(y_i \in \mathbb{R}\)\\
		& \(x_j \geq 0\) & \(j\)th constraint has \(\geq\)\\
		& \(x_j \leq 0\) & \(j\)th constraint has \(\leq\)\\
		& \(x_j \in \mathbb{R}\) & \(j\)th constraint has \(=\)\\
		\bottomrule
	\end{tabulary}
\end{table}

\section{Branch and Bound}

Consider a (hard to solve) optimization problem
\begin{align}
	\min f\left(x\right) &\text{s.\,t. } x \in M\text{.}\\
	\shortintertext{Associate a \emph{relaxed}\index{relaxed} optimization problem }
	\min g\left(x\right) &\text{s.\,t. } x \in R
\end{align}
such that
\begin{itemize}
	\item \(M \subseteq R\),
	\item if \(x \in R\), then \(g\left(x\right) = f\left(x\right)\), and
	\item The relaxed problem can be solved efficiently.
\end{itemize}

Then, if \(y^{\ast}\) is an optimal solution for the relaxed problem,
then \(f\left(x\right) \geq f\left(y^{\ast}\right)\) for all \(x \in M\),
i.\,e. the optimal solution of the relaxed problem is an
upper bound for the optimal solution of the hard problem.
If \(y^{\ast} \in M\), then \(y^{\ast}\) is also an optimal solution
to the hard problem.

The idea of \emph{branch and bound}\index{branch and bound}
is now to partition the input space \(M\) into \(M_1\) and \(M_2\)
and solve the optimization problem for both \(M_1\) and \(M_2\) individually
(possibly splitting these problems up again),
compare both solution and choose the better one.
This solution then is the optmial solution for \(x \in M\).

When solving for some input space \(M_i\),
first solve the relaxed problem \(g\).
If it has an optimal solution \(y^{\ast} \in M_i\),
then immediately return this solution as solution for the hard problem.
Otherwise, split up \(M_i\) into two partitions as describe above
and solve those.

If the optimal solution for \(g\) in each step turns out ot be worse
than the currently known best solution,
don't continue on this path at all
(as the solution can only get worse than the currently known best solution).

The branch and bound method recursively splits up the input space,
and obtains optimal solutions for each part of the partition,
then chooses the best one.
To reduce calculation effort and eventually obtain a solution,
the relaxed problem is solved in each step
and check for whether it yields a solution that matches one input from
the current input space.
Furthermore, if at some point it is clear that the solution for the relaxed problem
is worse than the best known solution,
the solution for the hard problem can only get worse down this recursion path,
so calculation stops there.
Eventually, all branches have been either checked or discarded
and the optimal solution has been obtained.
However, in the worst case, the whole input space has been searched,
leading to extremely large run times.

\subsection{Branch and Bound for Mixed Integer Problems}

Consider the MIP
\begin{align}
	\min f\left(\mathbf{x}\right) &= \mathbf{c}\mathbf{x}\\
	\shortintertext{s.\,t.}
	\mathbf{A}\mathbf{x} &= \mathbf{b}\\
	\mathbf{x} &\geq \mathbf{0}
\end{align}
And \(x_i\) is integer for all \(j \in I\)
where \(\mathbf{x} = \left(x_1, \dotsc, x_n\right)\),
\(\mathbf{b} \geq \mathbf{0}\), \(\mathbf{A} \in \mathbb{R}^{m \times n}\)
has rank \(m \leq n\)
and \(I \subseteq \left\{1, \dotsc, n\right\}\).

Thus, some subset of variables \(x_i\) must be integer.

A relaxed problem to this hard problem can be chosen
by simply removing the integer constraint
(i.\,e.~the \(x_i\) that must be integer in the original problem may now be real).
This is called \emph{LP-relaxation}\index{LP-relaxation}
and results in the following LOP:
\begin{align}
	\min f\left(\mathbf{x}\right) &= \mathbf{c}\mathbf{x}\\
	\shortintertext{s.\,t.}
	\mathbf{A}\mathbf{x} &= \mathbf{b}\\
	\mathbf{x} &\geq \mathbf{0}
\end{align}
where \(\mathbf{x} = \left(x_1, \dotsc, x_n\right)\) and
\(\mathbf{b} \geq \mathbf{0}\), \(\mathbf{A} \in \mathbb{R}^{m \times n}\)
has rank \(m \leq n\).

The relaxed problem can be solved with the Simplex Algorithm,
while the MIP must be solved with branch and bound.
During the branch step, the input space is split up
by choosing some integer variable \(y_k\) and
its optimal value \(y_k^{\ast}\) for the relaxed problem.
The two sets of possible input values are now:
\begin{align}
	B_1 &= \left\{y \in B \mmid y_k \leq \left\lfloor y_k^{\ast} \right\rfloor\right\}\\
	B_2 &= \left\{y \in B \mmid y_k \geq \left\lfloor y_k^{\ast} \right\rfloor\right\}
\end{align}
if \(B\) is the input space from the “parent” operation.

\subsection{Cutting planes for MIP}

Given a MIP and its LP-relaxation as before,
the idea is to find a valid cut \(\mathbf{d}\mathbf{x} \leq q\) such that
\begin{align}
	\left\{\mathbf{x} \mmid \mathbf{A}\mathbf{x} = \mathbf{b}, \mathbf{x} \geq \mathbf{0}, \mathbf{d}\mathbf{x} \leq q\right\} \neq R \text{ and}\\
	\left\{\mathbf{x} \mmid \mathbf{A}\mathbf{x} = \mathbf{b}, \mathbf{x} \geq \mathbf{0}, x_j \text{ is integer for all } j \in I, \mathbf{d}\mathbf{x} \leq q\right\} = M
\end{align}
Such a cut reduces \(M\) by the optimal solution of the LP-relaxation.
Thus, running the solver for the LP-relaxation again now gives a different
optimal solution, which hopefully is closer to the optimal integer solution.
This is process is repeated until the optimal solution to the LP-relaxation
is integer,
in which case it is also an optimal solution to the MIP.
