\chapter{Network Design Problems}
\section{Simple Design Problem}\index{Simple Design Problem}

\begin{itemize}
	\item Indices:
		\begin{itemize}
			\item \(d = 1, 2, \dotsc, D\)\ldots{} demands
			\item \(p = 1, 2, \dotsc, P_d\)\ldots{} candidate aths for flows
				realizing demand \(d\)
			\item \(e = 1, 2, \dotsc, E\)\ldots{} links
		\end{itemize}

	\item Constants:
		\begin{itemize}
			\item \(\delta_{edp}\)\ldots{}
				if link \(e\) belongs to path \(p\) realizing demand \(d\)
			\item \(h_d\) volume of demand \(d\)
			\item \(\xi_e\) unit (marginal) cost of link \(e\)
			\item \(x_{dp}\) flow allocated to path \(p\) of demand \(d\)
			\item \(y_e\) capacity of link \(e\)
		\end{itemize}

	\item Objective: \(\min \mathbf{F} = \sum_e \xi_e y_e\) (bandwidth cost)
	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d,
				& \forall d &\in \left\{1, \dotsc, D\right\} \text{ (demand constraints)}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp} &\leq y_e
				& \forall e &\in \left\{1, \dotsc, E\right\} \text{ (capacity constraints)}
		\end{align}
\end{itemize}
Solutions to this optimization problem will have \(y_e\) equal to the load on the links,
as otherwise the costs could be lowered (and thus the solution wouldn't be optimal).
\(y_e\) can be substituted in the cost function with
\(y_e = \sum_{d}\sum_{p}\delta_{edp}x_{dp}\) and
\(\zeta_{dp} = \sum_{e} \xi_e \delta_{edp}\) denoting
the cost of path \(p\) for demand \(d\):
\begin{align}
	\mathbf{F} &= \sum_{d=1}^{D}\sum_{p=1}^{P_d}\zeta_{dp}x_{dp}
\end{align}

This leads to the SDP-Decoupled Link-Path formulation (SDP/DLPF):
\begin{itemize}
	\item Variables: \(x_{dp}\) flow variable allocated to path \(p\) of demand \(d\)
	\item Objective: \(\min \mathbf{F} = \sum_{d=1}^{D} \sum_{p=1}^{P_d} \zeta_{dp}x_{dp}\)
	\item Constraints:
		\(\forall d \in \left\{1, \dotsc, D\right\}: \sum_{p=1}^{P_d} x_{dp} = h_d\)
\end{itemize}
This problem formulation has fewer variables!
Due to the linear structure of \(\mathbf{F}\),
this is actually a set of decoupled optimization problems,
with an optimal solution to these problems allocating all demand on the shortest path.
If there are multiple shortest paths,
the demand can be arbitrarily split among these paths.

\section{Capacitated Problems}
\subsection{Pure Allocation Problem}
\index{Pure Allocation Problem}
\begin{itemize}
	\item Constants:
		\begin{itemize}
			\item \(\delta_{edp}\) as before
			\item \(h_d\) as before
			\item \(c_e\)\ldots{} capacity of link \(e\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(x_{dp}\) as before
		\end{itemize}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d \in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e
				& \forall e \in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
This problem has no objective function to optimize,
so in this basic form the goal is just to find \emph{any} solution
that satisfies the constraints.

Another auxiliary variable can be introduced to define an objective.
This yields the PAP Modified Link Path Formulation:
\index{PAP Modified Link Path Formulation}
\begin{itemize}
	\item Variables:
		\begin{itemize}
			\item \(x_{dp}\) as before
			\item \(z\)\ldots{} auxiliary continuous variable (of unrestricted sign)
		\end{itemize}
	\item Objective:
		\begin{align}
			\min z
		\end{align}
	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq z + c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
This problem always has a solution and
if \(z^{\ast} \leq 0\), then \(x_{dp}^{\ast}\) represent a solution to the original PAP.
If PAP is feasible, then a solution \(\mathbf{x}\) with
at most \(D+E\) non-zero flows exists.

In many cases, some objective function to find the best solution shall be found.
The above problem maximizes the minimum unused capacity.
However, it is also possible to maximize the total unused capacity
after flow allocation:
\begin{align}
	\max \mathbf{F} &= \sum_{e=1}^{E} r_e\left(
		c_e - \sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp}
		\right)\\
		&= \sum_{e=1}^{E} r_e\left(c_e - \underline{y}_e\right)
\end{align}

\subsection{Bounded Link Capacities}\index{Bounded Link Capacities}
A variation of a mixed dimensioning/capacitated problem,
where link capacities \(y_e\) shall be dimensioned with upper bounds \(c_e\)
can also be considered:
\begin{itemize}
	\item Objective:
		\begin{align}
			\min F &= \sum_{e=1}^{E} \xi_e y_e
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq y_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			y_e &\leq c_e & \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

\subsection[Path Diversity]{Path Diversity (PD)}\index{Path Diversity}
Sometimes, flows shall be allocated in a way that
no single path flow \(x_{dp}\) carries more than a fraction of the demand
(expressed by \(n_d\)\ldots{} the number of different paths among
which \(h_d\) is to be split).

\begin{itemize}
	\item Variables:
		\begin{itemize}
			\item \(x_{dp}\) as before
		\end{itemize}
	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			x_{dp} &\leq \frac{h_d}{n_d}
				& \forall d &\in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}
		\end{align}
\end{itemize}
If \(n_d\) is an integer,
it will force the demand \(d\) to be split amon onto at least \(n_d\) different paths.
If all \(P_{d}\) candidate paths for a demand \(d\) are link disjoint,
this can guarantee that a single link failure leads to demand \(d\)
loosing at most \(\frac{100\,\%}{n_d}\) of its volume.

\subsection[Generalized Diversity]{Generalized Diversity (GD)}
\index{Generalized Diversity}

The diversity constraint from PD can also be formulated in a stricter way,
allowing to pass arbitrary candidate path lists\footnote{
	Note the inclusion of \(\delta_{edp}\) in the last constraint set here.
}:
\begin{itemize}
	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			\sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq \frac{h_d}{n_d}
				& \forall d &\in \left\{1, \dotsc, D\right\}, e \in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
The modified constraint ensures that no link \(e\) of path \(P_{dp}\)
carries more than \(\frac{100\,\%}{n_d}\) of demand \(d\).
As multiple candidate paths for one demand may use one specific link,
the sum over all candidate paths must be calculated.
A major drawback of this problem is the high number of constraints.

The shortest path allocation rule from the original problem
can be used in a modified version:
\begin{itemize}
	\item First, look up the shortest path for a demand an allocate \(\frac{h_d}{n_d}\) to it.
	\item Then, allocate the next fraction \(\frac{h_d}{n_d}\) the next shortest path
		and so on.
\end{itemize}

\subsection{Lower Bounded Flows}\index{Lower Bounded Flows}

Sometimes, the flow over a path shall be restricted from below
to avoid having a flow to be partitioned among too many paths.
This is somehow opposite to path diversity.
It requires modeling a lower bound to non-zero flows.
thus, new constants and binary variables need to be introduced,
leading to the followig problem:
\begin{itemize}
	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			x_{dp} &\leq h_du_{dp}
				& \forall d &\in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}\\
			b_du_{dp} &\leq x_{dp}
				& \forall d &\in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}
		\end{align}
\end{itemize}
The binary variables \(u_{dp}\) make this problem difficult to solve,
as most methods are not significantly different from
trying out all combinations.

\subsection{Limited Demand Split}\index{Limited Demand Split}

The idea is to limit among how many non-zero path flows a demand \(d\) can be split,
represented by the constant \(k_d\).
Dependig on \(k_d\), this can result in a number of optimization problems
and always introduces binary variables \(u_{dp}\).

Single-Path-Allocation (SPA, \(k_d = 1\))\index{Single-Path-Allocation}:
\begin{itemize}
	\item Constraints:
		\begin{align}
			x_{dp} &= h_d u_{dp}
				& \forall d &\in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}\\
			\sum_{p=1}^{P_d} u_{dp} &= 1
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
The second constraint enforces that for each demand
exactly one binary variable \(u_{dp} = 1\) and all others are \(= 0\).
The first constraint enforces that \(x_{dp} = 0\) whenever \(u_{dp} = 0\),
i.\,e.~non-zero flows can only exists for paths and demands where \(u_{dp} = 1\).

This problem is known to be NP-complete.

\paragraph*{Integral Flow Pure Allocation Problem:}
\index{Integral Flow Pure Allocation Problem}
For a set of demands find an integral solution
(all \(x_{dp}\) are integers) so that capacity constraints of all edges
are not exceeded.

The SPA formulation can be simplified by eliminating flow variables \(x_{dp}\):
\index{Single-Path Allocation}
\begin{itemize}
	\item Variables:
		\begin{itemize}
			\item \(u_{dp}\)\ldots{} binary variable, flow allocated to path \(p\) of demand \(d\)
		\end{itemize}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} u_{dp} &= 1
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D}h_d \sum_{p=1}^{P_d} \delta_{edp}u_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

By using binary variables, the formulation that a demand \(d\)
must be split equally among \(k_d\) candidate paths is also possible:

\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(k_d\)\ldots{} predetermined number of paths for demand \(d\)
		\end{itemize}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} u_{dp} &= k_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \left(
				\sum_{p=1}^{P_d} \delta_{edp}u_{dp}
			\right) \frac{h_d}{k_d} &\leq c_e
				& e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

Arbitrary Split Among \(k\) Paths:
\index{Arbitrary Split Among k Paths@Arbitrary Split Among \(k\) Paths}
\begin{itemize}
	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{p=1}^{P_d} u_{dp} &= k_d
				& d &\in \left\{1, \dotsc, D\right\}\\
			x_{dp} &\leq u_{dp}h_d
				& d &\in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e
				& e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

\section{Modular Flow Allocation}\index{Modular Flow Allocation}

In transport networks, demand volumes are usually given in terms of modular units,
as the underlying technology normally only allows certain
discrete network speeds (e.\,g.SFP modules, nBASE-T standards).
In such cases, demands \(d\) can be modeled as a number \(H_d\)
of demand modules, each with capacity \(L_d\) (thus \(h_d = L_d \cdot H_d\)).

\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(L_d\)\ldots{} demand module for demand \(d\)
			\item \(H_d\)\ldots{} volume of demand \(d\) expressed as
				the number of demand modules
		\end{itemize}

	\item Constraints:
		\begin{align}
			x_{dp} &= L_d u_{dp}
				& \forall d &\in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}\\
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc D\right\}\\
			\sum_{d=1}^{D}\sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
As \(x_{dp} = L_d \cdot u_{dp}\), the variables \(x_{dp}\) can be eliminated
to simplify the formulation:

\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(L_d\)\ldots{} demand module for demand \(d\)
			\item \(H_d\)\ldots{} volume of demand \(d\) expressed as the number of demand modules
		\end{itemize}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} u_{dp} &= H_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} L_d \sum_{p=1}^{P_d} \delta_{edp}u_{dp} &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

\section{Non-Linear Link Dimenioning, Cost and Delay Functions}

So far, the assumption was that link capacities are equal to
the link loads for uncapacitated problems.
However, typically the link cost function is built upon the notion of
the link dimensioning function \(F_e\left(\underline{y}_e\right)\)
which determines the relationship between the link load \(\underline{y}_e\) and
the minimal required link capacity \(y_e\).

As the link cost was computed as the capacity times a cost coefficient \(\xi_e\),
the link cost was always considered linear.
The following problems extend this by considering also module links,
links with convex cost and links with concave cost.

\subsection{Modular Links}

In practice, links are often of modular size.
Assume that the size of one
\emph{link capacity module}\index{link capacity module} is \(M\).
The variable \(y_e\) denotes the number of link capacity modules.
The link dimensioning function is:
\begin{align}
	F_e\left(\underline{y}_e\right) &= k\xi_e \text{ with }
	\left(k-1\right)M \leq \underline{y}_e \leq kM
\end{align}
This gives the optimization problem for \emph{modular links}\index{modular links} (ML):

\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(\xi_e\)\ldots{} cost of one capacity module on link \(e\)
			\item \(M\)\ldots{} unique size of the link capacity module
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e=1}^{E} \xi_ey_e
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp} &\leq M y_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
The dimensioning problem with modular links ML is NP-complete.

This problem could be solved heuristically by assuming a linear approximation
of the link dimensioning function \(F_e\),
solving the respective linear programming problem and then
rounding up the obtained link capacities.
This, however, can lead to solutions that are far from optimal.

ML can also be generalized to cover multiple module sizes \(M_1, \dotsc, M_K\)
where \(K\) Is the number of module types and variable \(y_{ek}\)
denotes the number of modules of size \(M_k\) installed on link \(e\):
Links With Multiple Modular Sizes (LMMS)
\index{Links With Multiple Modular Sizes}:

\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(\xi_{ek}\)\ldots{} cost of one capacity module of type \(k\) on link \(e\)
			\item \(M_k\)\ldots{} size of the link capacity module of type \(k\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e=1}^{E} \sum_{k=1}^{K} \xi_{ek}y_{ek}
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq
				\sum_{k=1}^{K} M_{k}y_{ek}
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
Modeling \(K\) different module sizes increases the number of required variables
by a factor of \(K\),
as one set of variables is needed for each modular unit type.
Yet another way of introducing module cost function with different modules
is the incremental characterization:
\begin{itemize}
	\item \(K\) denotes the number of steps.
	\item The incremental sizes of the link capacity module of type \(k\)
		are modeled with \(m_1, m_2, \dotsc, m_k\)
		(if the load on a link passes one of these values,
		the respective cost function for this link “jumps”).
	\item The cost of each incremental module \(m_k\) on link \(e\) is \(\xi_{ek}\).
	\item Binary variables \(u_{ek}\) are used to indicate whether
		the incremental module of type \(k\) is installed on link \(e\) or not.
\end{itemize}
This gives the problem \emph{Links With Incremental Modules} (LIM):
\index{Links With Incremental Modules}
\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(\xi_{ek}\)\ldots{} cost of one capacity module of type \(k\) on link \(e\)
			\item \(m_k\)\ldots{} incremental size of the link capacity module of type \(k\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(x_{dp}\)\ldots{} flow allocated to path \(p\) of demand \(d\)
			\item \(u_{ek}\)\ldots{} binary variable indicating if module of type \(k\)
				is installed on link \(e\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e=1}^{E}\sum_{k=1}^{K} \xi_{ek}u_{ek}
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq \sum_{k=1}^{K}m_k u_{ek}
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			u_{e1} \geq u_{e2} \geq \dotsc &\geq u_{eK}
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

\subsection{Convex Cost and Delay Functions}

A real-valued function \(f\) defined on the interval \(\left[0, \infty\right)\)
is called \emph{convex}\index{convex},
if for any two points \(z_1, z_2 \in \left[0, \infty\right)\) and
any \(\alpha \in \left[0, 1\right]\) we have:
\begin{align}
	\alpha f\left(z_1\right) + \left(1 - \alpha\right)f\left(z_2\right) &\geq
	f\left(\alpha z_1 + \left(1-\alpha\right) z_1\right)
\end{align}
Pictorically, a function is called convex if the function lies below or on
the straight line segment connecting two points,
for any two points in the interval.

\emph{Allocation With Convex Cost Function}\index{Allocation With Convex Cost Function}
(CCF):
\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(F_e\left(\cdot\right)\)\ldots{} convex cost function of link \(e\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(\underline{y}_e\)\ldots{} load of link \(e\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e=1}^{E} F_e\left(\underline{y}_e\right)
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &= \underline{y}_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			\underline{y}_e &\leq c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}

Convex function can be used to convert capacitated flow allocation problems
to uncapacitated ones by using \emph{penalty functions}\index{penalty function}.
This requires that the penalty function is convex and incurs a high cost
if the link capacity is violated.
The uncapacitated problem can then be obtained by omitting the constraints
\begin{align}
	\underline{y}_e &\leq c_e & \forall e &\in \left\{1, \dotsc, E\right\}
\end{align}

To solve convex optimization problems with the techniques learned so far,
linear approximations of the convex functions must be made
to obtain a “corresponding” linear problem.
In the general case,
the convex function is approximated with a series \(g_k\left(z\right)\) of linear functions:
\begin{itemize}
	\item \(g\left(z\right) = g_k\left(z\right) = a_kz + b_k\)
	\item \(s_{k-1} \leq z \leq s_k, k \in \left\{1, \dotsc, K\right\}\)
	\item \(s_1 = 0, s_k = \infty\)
\end{itemize}
As linear programming problems have their optimum solutions
in the edges of the polytope describing the area of valid solutions,
the solution computed will be a point where
the linear approximation is equal to the actual convex function.
Thus, an optimal solution to the approximative problem
is also a valid solution to the original problem.
However, it is not necessarily an optimal solution!
Different approximations can lead to different solutions.

\emph{Convex Penalty Function with Piecewise Linear Approximation}
\index{Convex Penalty Function with Piecewise Linear Approximation} (CPF/PLA):
\begin{itemize}
	\item Additional incdices and constants:
		\begin{itemize}
			\item \(k = 1, \dotsc, K_e\)\ldots{} consecutive pieces of the linear approximation
				of \(F_e\left(\cdot\right)\)
			\item \(a_{ek}, b_{ek}\)\ldots{} coefficients of the linear pieces of
				the linear approximation of \(F_e\left(\cdot\right)\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(r_e\)\ldots{} continuous variable approximating
				\(F_e\left(\underline{y}_e\right)\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e=1}^{E} r_e
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp} &= \underline{y}_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			r_e &\geq a_{ek}\underline{y}_e + b_{ek}
				& \forall e &\in \left\{1, \dotsc, E\right\}, k \in \left\{1, \dotsc, K_e\right\}
		\end{align}
\end{itemize}
Thus, convex mathematical programming problems
can be transformed into linear programming problems.
Also, in many applications,
it is not important to know the exact equations of the linear pieces
of the approximation,
but only the slopes \(a_{ek}\) and points \(s_k\) where they change matter.

\begin{align}
	&\min a_1z_1 + a_2z_2 + \dotsc + a_Kz_K\\
	\shortintertext{subject to}
	y &= z_1 + z_2 + \dotsc + z_K\\
	0 &\leq z_1 \leq s_1 - s_0\\
	0 &\leq z_2 \leq s_2 - s_1\\
	&\vdots\\
	0 &\leq z_K \leq s_K - s_{K-1}
\end{align}
This works, because due to convexity we have \(a_1 < a_2 < \dotsc < a_K\).

Problems with a linear cost function, but convex constraints are also possible,
e.\,g.~minimizing capacity cost for a fixed routing
under the constraint that a given average acceptable delay \(\hat{D}\) has to be met.
This can be solved by various methods:
\begin{itemize}
	\item Karush-Kuhn-Tucker conditions
	\item Classical Lagrangian multiplier method
	\item Piecewise linear approximation as described before
\end{itemize}

\emph{Capacity Design With Fixed Routing and Delay Constraint}:
\index{Capacity Design With Fixed Routing and Delay Constraint}
\begin{itemize}
	\item Constants:
		\begin{itemize}
			\item \(\underline{y}_e\)\ldots{} load on link \(e\) induced by fixed routing
			\item \(\hat{D}\)\ldots{} acceptable delay
			\item \(H\)\ldots{} total traffic volume \(H = \sum_{d} H_d\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(y_e\)\ldots{} capacity of link \(e\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e}^{E} \xi_e y_e
		\end{align}

	\item Constraints:
		\begin{align}
			y_e &\geq \underline{y}_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			\frac{1}{H} \sum_{e}^{E} \frac{y_e}{y_e - \underline{y}_e} &\leq \hat{D}
		\end{align}
\end{itemize}

\subsection{Concave Link Dimensioning Functions}

A real-valued function \(f\) defined on the interval \(\left[0, \infty\right)\)
is called \emph{concave}\index{concave},
if for any two points \(z_1, z_2 \in \left[0, \infty\right)\)
and any \(\alpha \in \left[0, 1\right]\)
we have:
\begin{align}
	\alpha f\left(z_1\right) + \left(1-\alpha\right)f\left(z_2\right)
	&\leq f\left(\alpha z_1 + \left(1-\alpha\right) z_2\right)
\end{align}

Pictorically, a function is called concave
if the function lies above or on the straight line segment
connecting two points,
for any two points in the interval.

In networks, concave functions often appear to describe link dimensioning functions,
as growth in link costs often adheres to the following relation:
\begin{align}
	\frac{f\left(z_1\right)}{z_1} &\geq \frac{f\left(z_2\right)}{z_2} \text{ for } z_1 < z_2
\end{align}

\emph{Concave Link Dimensioning Functions}\index{Concave Link Dimensioning Functions}
(CDF):
\begin{itemize}
	\item Additional constants:
		\begin{itemize}
			\item \(F_e\left(\cdot\right)\)\ldots{} non-decreasing concave dimensioning function
				of link \(e\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(\underline{y}_e\)\ldots{} load of link \(e\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e}^{E} \xi_e F_e\left(\underline{y}_e\right)
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D}\sum_{p}^{P_d} \delta_{edp}x_{dp} &= \underline{y}_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
		\end{align}
\end{itemize}
Optimal solutions to these kinds of problems are non-bifurcated
as allocating one big link is cheaper than allocating two small links.
As these problems require minimizing a concave objective function
subject to linear constraints,
they in general can have numerous local minima
on the extreme points of the feasible region defined by the constraints.
Thus, finding the global minimum can be a very difficult task.

Piecewise linear approximation as was done for convex functions
does not lead to a linear programming problem for concave functions.
However, approximation leads to a mixed-integer programming program.
In general:
\begin{align}
	g\left(z\right) &:= g_k\left(z\right) = a_k z + b_k,
	s_{k-1} \leq z < s_k, k \in \left\{1, \dotsc, K\right\}
\end{align}
To avoid multiplying two variables
(which is forbidden in mixed integer programming problems),
additional variables \(y_k\) must be introduced
and limited by additional constraints:
\begin{align}
	\min \sum_{k=1}^{K}\left(a_k y_k + b_k u_k\right)&\\
	\shortintertext{subject to}
	\sum_{k}^{K}y_k &= y\\
	y_k &\leq \Delta u_k
		\qquad \forall k \in \left\{1, \dotsc, K\right\}\\
	y_k &\quad\text{nonnegative continuous}\\
	u_k &\quad\text{binary}\\
	\Delta &\quad\text{number larger than any potential value } y
\end{align}
These constraints force that exactly one value
(the right one) \(y_k\) will be non-zero and equal to \(y\)
in the optimal solution.

\emph{Concave Dimensioning Functions with Piecewise Mixed-Integer Approximation}
\index{Concave Dimensioning Functions with Piecewise Mixed-Integer Approximation}
(CDF/\allowbreak{}PMIA):
\begin{itemize}
	\item Additional Indices and Constants:
		\begin{itemize}
			\item \(k = 1, \dotsc, K_e\)\ldots{} consecutive pieces of the linear approximation
			\item \(a_{ek}, b_{ek}\)\ldots{} coefficients of the linear pieces of the approximation
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(y_{ek}, u_{ek}\) continuous/binary variables for link \(e\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e}\sum_{k} \left(a_{ek} y_{ek} + b_{ek} u_{ek}\right)
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &= \underline{y}_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			\sum_{k=1}^{K} y_{ek} &= \underline{y}_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			\sum_{k=1}^{K} u_{ek} &= 1
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
			y_{ek} &\leq \Delta u_{ek}
				& \forall e &\in \left\{1, \dotsc, E\right\}, k \in \left\{1, \dotsc, K\right\}
		\end{align}
\end{itemize}

Assuming that the piecewise linear approximation involves the same number \(K\) of pieces
for every link, the above problem contains:
\begin{itemize}
	\item \(E \cdot K\) additional continuous variables \(y_{ek}\)
	\item \(E \cdot K\) additional binary variables \(u_{ek}\)
	\item \(E \cdot \left(K + 2\right)\) additional constraints
\end{itemize}
This indicates that the problem is difficult to solve.
Ther are no algorithms known that are significantly better than
the full search in the space of binary variables.

Similar to convex functions, sometimes just looking at slopes and
the points where the slopes change might be enough instead of doing a full approximation.

\section{Budget Constraints}

A possible alternative optimization goal to minimizing cost is
to stay within a given budget constraint and choose
a different optimization goal,
e.\,g.~when optimizing for throughput, while staying in budget constraints:

\emph{Budget Constraint}\index{Budget Constraint} (BC):

\begin{itemize}
	\item Additional Constants:
		\begin{itemize}
			\item \(B\)\ldots{} given budget
			\item \(h_d\)\ldots{} reference volume of demand \(d\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(y_e\)\ldots{} capacity of link \(e\)
			\item \(r\)\ldots{} proportion of the realized demand volumes
		\end{itemize}

	\item Objective:
		\begin{align}
			\max r
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &\geq rh_d
				& \forall d &\in \left\{1, \dotsc, D\right\}
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq y_e
				& \forall e &\in \left\{1, \dotsc, E\right\}
			\sum_{e=1}^{E} \xi_ey_e &\leq B
		\end{align}
\end{itemize}

\section{Incremental Network Design Problems}

Often networks are not designed from scratch,
but have to be extended with additional resources.
In such cases, there are already existing link capacities \(c_e\)
and the task is to add additional capacities \(y_e\)
to account for an increase in the demand volume.

For this, add to the existing problem capacity constraints
for all links which are already present.
The cost of such a problem is generally
higher than the optimal solution of a pure network design problem.

\emph{Simple Extension Problem}\index{Simple Extension Problem} (SEP):
\begin{itemize}
	\item Additional Constants:
		\begin{itemize}
			\item \(c_e\)\ldots{} existing capacity of link \(e\)
			\item \(\xi_e\)\ldots{} unit cost of link \(e\)
		\end{itemize}

	\item Variables:
		\begin{itemize}
			\item \(y_e\)\ldots{} extra capacity of link \(e\) on top of \(c_e\)
		\end{itemize}

	\item Objective:
		\begin{align}
			\min \mathbf{F} &= \sum_{e=1}^{E} \xi_e y_e
		\end{align}

	\item Constraints:
		\begin{align}
			\sum_{p=1}^{P_d} x_{dp} &= h_d
				& \forall d &\in \left\{1, \dotsc, D\right\}\\
			\sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp} x_{dp} &\leq y_e + c_e
				& \forall e &\in \left\{1, \dotsc, E\right\}\\
		\end{align}
\end{itemize}

\section{Representing Nodes}

So far, only link capacities were considered,
but node capacities can be limited or imply costs as well.
These can be modeled by splitting up each node
into an ingress node, an egress node and a connecting internal link.
All ingress links are connected to the ingress node
and all egress links are connected to the egress node.
The link capacity of the internal link can then be used
to model the capacity of the node from the original graph.

This can be used both for limiting capacity constraints on nodes
or node failures (by failing its internal link).
