\chapter{Modeling Network Design Problems}

\section{Link-Path Formulation}\index{link-path formulation}

\begin{itemize}
	\item \(\hat{h}_{12} = 5\)\ldots{} undirected demand between node \(1\) and \(2\) is \(5\), also noted as \(\left<1, 2\right>\)
	\item \(\hat{x}_{132}\)\ldots{} amount of flow over path 1, 3, 2
	\item \(\hat{c}_{12}\)\ldots{} capacity of link 1--2
	\item \(a^{\ast}\)\ldots{} optimal solution for variable \(a\) (e.\,g.~\(\hat{x}_{132}^{\ast}\))
\end{itemize}
These can be combined to obtain systems of equations,
which usually have multiple solutions.
The answer to the question, which of these solutions is of best interest,
depends on the goal of network design, e.\,g.:
\begin{itemize}
	\item Minimize the total routing cost (if links are annotated with a link cost)
	\item Minimize congestion of the most congested link
\end{itemize}

If the objective is to minimize the total routing cost and
the cost of routing one unit of traffic over one link is set to 1 for all links
(i.\,e.~the goal is to minimize the number of hops for each route),
an objective function might be:
\begin{align}
	\mathbf{F} &= \hat{x}_{12} + 2\hat{x}_{132} + \hat{x}_{13} + 2\hat{x}_{123} + \hat{x}_{23} + 2\hat{x}_{213}
\end{align}
Note that flows routed over two links are weighted with factor 2.
Such an optimization task is called a
\emph{multi-commodity flow problem}\index{multi-commodity flow problem}.
The inverse objective (try to avoid direct links) can also occur,
e.\,g.~in air travel networks.

Link-path formulation is one of multiple ways to describe network optimization problems.
It is appropriate for networks with undirected links as well as with directed links.

\section{Node-Link Formulation}\index{node-link formulation}

In this scenario, links and demands are assumed to be directed,
so a link 1--2 is substituted with two directed links (“\emph{arcs}\index{arc}”)
\(1\to2\) and \(2\to1\).
Instead of tracing all path flows realising the demand,
the total link flow for the demand on each link is considered.
Undirected demands \(\left<1,2\right>\) are replaced
with directed demands \(\left<1:2\right>\).

Looking from the point of view of a fixed node that is not end point of the flow,
there are flows coming in and going out of that node.
The total incoming flow must then be equal to the total outgoing flow.
The demand of source nodes is the total outgoing flow minus the total incoming flow,
while for sink nodes the demand is the total incoming flow minus the total outgoing flow.

This gives the following notation:
\begin{itemize}
	\item \(\tilde{x}_{13,12}\)\ldots{} flow over arc \(1\to3\) for demand \(\left<1:2\right>\)
\end{itemize}

For each demand and each node, all possible direction of paths (including backflows)
are part of the total flow equation, although backflows can be safely set to 0,
as they make no sense from a practical viewpoint.
Additionally, the source node includes a \(-\hat{h}\) term
and the sink node includes a \(\hat{h}\) term to account for
the imbalance in incoming/outgoing flow caused by the demand
(account for the conservation of flow).

A simple example for a demand \(\left<1:2\right>\) with nodes \(1,2,3\) might be:
\begin{align}
	\tilde{x}_{12,12} + \tilde{x}_{13,12} &= \hat{h}_{12}\\
	-\tilde{x}_{13,12} + \tilde{x}_{32,12} &= 0\\
	-\tilde{x}_{12,12} - \tilde{x}_{32,12} &= -\hat{h}_{12}
\end{align}

Capacity constraints are modeled as before:
\begin{align}
	\tilde{x}_{12,12} + \tilde{x}_{12,13} &\leq \hat{c}_{12}
\end{align}

Note that the two notations shown until now can become very cumbersome for larger networks:
\begin{itemize}
	\item There might be no demand between some or many pairs of nodes
	\item There are no links between most pairs of nodes
\end{itemize}
Still, the two formulations would require inclusion of these cases,
although they are not relevant to the solution at all.

\section{Link-Demand-Path-Identifier-Based Notation}
\index{link-demand-path-identifer-based formulation}

This formulation assignes indices to demands and capacities,
yielding a simpler notation:
\begin{itemize}
	\item \(h_i\)\ldots{} the demand with index \(i\)
	\item \(c_j\)\ldots{} the (known) capacity of the \(j\)th link
	\item \(y_j\)\ldots{} the unknown capacity of the \(j\)th link (dimensioning problem)
	\item \(x_{ij}\)\ldots{} the flow of demand \(i\) over link \(j\)
	\item \(v_1-v_2-\dotsc-v_{n+1}\)\ldots{} undirected path in node representation
	\item \(v_1\to v_2\to \dotsc \to v_{n+1}\)\ldots{} directed path in node representation
	\item \(\left\{e_1, e_2, \dotsc, e_n\right\}\)\ldots{} undirected path in link representation
	\item \(\left(e_1, e_2, \dotsc, e_n\right)\)\ldots{} directed path in link representation
	\item \(\xi_{i}\)\ldots{} cost of link \(e_i\)
	\item \(P_{ij}\)\ldots{} candidate path \(j\) for demand \(i\)
	\item \(\delta_{edp} : e \times P_{ij} \to \left\{0, 1\right\}\)\ldots{} is link \(e_k\) on candidate path \(P_{ij}\), \emph{link-path incidence relation}\index{link-path incidence relation}
\end{itemize}

The vector of all flows is called the \emph{flow allocation vector}\index{flow allocation vector}
or \emph{flow vector}\index{flow vector}:
\begin{align}
	\mathbf{x} &= \left(\mathbf{x}_1, \mathbf{x_2}, \dotsc, \mathbf{x}_D\right)\\
	&= \left(x_{11}, x_{12}, \dotsc, x_{1P_1}, \dotsc, x_{D1}, x_{D2}, \dotsc, x_{DP_D}\right)\\
	&= \left(x_{dp} \mmid d = 1, 2, \dotsc, D; p = 1, 2, \dotsc, P_d\right)
\end{align}

The table for the link-path incidence relation \(\delta_{edp}\) contains a \(1\)
whenever a link \(e\) is used for satisfying a demand \(d\) over a path \(p\),
and \(0\) if the link is not used in that path.
Note that \(\delta_{edp}\) is not a variable, but fixed!

This us a notation for
the \emph{load}\index{load} \(\underline{y}_e\) of link \(e\)
and capacity constraints:
\begin{align}
	\underline{y}_e &= \sum_{d=1}^{D}\sum_{p=1}^{P_d} \delta_{edp}x_{dp}\\
	\forall e \in \left\{1, 2, \dotsc, E\right\}: \sum_{d=1}^{D}\sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq y_e
\end{align}

The general formulation of the simple dimensioning problem is:
\begin{align}
	\min \mathbf{F} &= \sum_{e = 1}^{E}\xi_e y_e &&\\
	\shortintertext{subject to}
	\forall d \in \left\{1, \dotsc, D\right\}: \sum_{p=1}^{P_d} x_{dp} &= h_d
	&&\text{demand}\\
	\forall e \in \left\{1, \dotsc, E\right\}: \sum_{d=1}^{D}\sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq y_e
	&&\text{capacity}\\
	\mathbf{x} &\geq 0
	&&\text{variables}\\
	\mathbf{y} &\geq 0 &&
\end{align}

Depending on whether link capacites are known (fixed) or unknown (to be chosen),
\(c_i\) and \(y_i\) are used, respectively.
Problems with unknown link capacities are referred to as
\emph{dimensioning problems}\index{dimensioning problem}
or \emph{uncapacitated problems}\index{uncapacitated problems},
contrary to \emph{capacitated problems}\index{capacitated problems}
where link capacities are known.
When variables can take continuous values,
then for any optimal solution, the capacity constraints become equalities,
as otherwise cost would arise for unused capacity (which is never optimal).

The \emph{cost}\index{cost} of a path \(P_{dp}\) is given by:
\begin{align}
	\zeta_{dp} &= \sum_{e=1}^{E}\delta_{edp}\xi_{e} \qquad d \in \left\{1, \dotsc, D\right\}, p \in \left\{1, \dotsc, P_d\right\}
\end{align}

\paragraph*{Shortest-Path Allocation Rule for Dimensioning Problems:}
For each demand, allocate its entire demand to its shortest path
with respect to link costs and candidate paths.
If there is more than one shortest path for a given demand,
then the demand volume can be arbitrarily split among shortest paths.

This rule works for simple dimensioning problems,
but might not work if further constraints are to be taken into account.
Further constraints might very well be imposed on the problem:
\begin{itemize}
	\item \emph{Non-bifurcated flows}\index{non-bifurcated} require
		each demand to be satisfied by exactly one path flow.
	\item To ensure graceful degradation in case of node or link failures,
		flows might need to be partitioned among several node-disjoint paths.
\end{itemize}
Depending on the demands and capacities,
non-bifurcated solutions might not even be possible, although bifurcated solutions exist.

\section{Capacitated Problems}

In some cases, link capacities are given and the task is to find a solution
that satisfies the specified demands, while staying within the capacity bounds.
Such problems can be formulated in the following general notation:
\begin{align}
	\forall d \in \left\{1, \dotsc, D\right\}: \sum_{p=1}^{P_d} x_{dp} &= h_d
	&&\text{demand constraints}\\
	\forall e \in \left\{1, \dotsc, E\right\}: \sum_{d=1}^{D}\sum_{p=1}^{P_d}
	\delta_{edp}x_{dp} &\leq c_e
	&&\text{capacity constraints}\\
	\mathbf{x} &\geq 0
	&&\text{constraints on variables}
\end{align}

Sometimes there might be no objective function,
rendering any feasible solution acceptable.
If flow routing cost is to be minimized, these problems are similar to the first problem.

\section{Shortest-Path Routing}

Shortest-Path routing is commonly used in networks.
Thus, the network design needs to anticipate that
demands will only be routed on their shortest paths.
The \emph{length}\index{length} of the path is determined
by adding up link costs \(w_e\) according to some weight system \(\mathbf{w}\).

Setting the link capacities to be equal to the computed link loads
of the shortest-path routing solution gives a (trivially) feasible solution.
In general, however, the objective is to find a solution
that allows to respect the given link capacities
and instead looks for the appropriate weight system.

\paragraph*{Single Shortest Path Allocation Problem}
For given link capacities \(\mathbf{c}\) and demand volumes \(\mathbf{h}\),
find a link weight system \(\mathbf{w}\) such that
the resulting shortest paths are unique and
the resulting flow allocation vector is feasible.

This problem is usually complex,
as non-bifurcated solutions may not exist even though bifurcated solutions do,
non-bifurcated solutions (if they exist) are usually hard to determine
and a weight system inducing an existing single-path flow solution
might be impossible to find.

If there are multiple shortest paths,
one might be interested in splitting demand volumes amoung multiple shortest paths.
Such a rule which is used in OSPF routing is the
\emph{equal-cost multi-path}\index{equal-cost multi-path}\index{ECMP} (ECMP) rule,
aiminng to equally split the outgoing demand volume over all outgoing next hops
with equal cost for a fixed destination.
However, such a simple might fail if link weights are not set appropriately.

\section{Fair Networks}

In the Internet, demands are often not fixed but \emph{elastic}\index{elastic},
meaning that each demand can consume any bandwidth assigned to its path.
In such a case, capacity constraints are given,
for the demands \(h_d\) no particular values are assumed.

An obviously initial solution is to assign each demand volume on its lower bound.
If this does not satisfy the capacity constraints,
there is no feasible solution at all.
If, however, feasibility is assured,
being able to carry more than the minimum required bandwidth
while at the same time giving a fair share of bandwidth to all flows might be desired.

The best-known general fairness criterion is
\emph{Max-Min-Fairness}\index{Max-Min-Fairness} (MMF\index{MMF}),
also called \emph{equity}\index{equity}:
\begin{itemize}
	\item If no lower bounds are specified,
		assign the same maximal value to all demands.
	\item If there is still capacity left,
		assign the same maximal value to all demands
		that can still make use of that capacity.
\end{itemize}

One might be interested in a compromise between MMF and
greedily maximizing network throughput.
One such fair allocation principle is
\emph{Proportional Fairness}\index{Proportional Fairness} (PF\index{PF})
and is realized by maximizing a logarithmic revenue function:
\begin{align}
	\forall d \in \left\{1, \dotsc, D\right\} \forall p \in \left\{1, \dotsc, P_d\right\}: \mathbf{R}\left(x\right) &= \sum_{d}r_d \ln\left(\sum_{p} x_{dp}\right)
\end{align}
with \(r_d\) being the revenue associated with demand \(d\).
If all demands are of equal importance, then \(r_d = 1\) for all demands \(d\).
This function is no longer linear.
However, it ensures that no demand is allocated an overall path flow sum of 0
and makes assigning (unfairly) high values “unattractive”.
By introducing a linear approximation of \(\mathbf{R}\),
the PF problem can be solved, as is shown later.

Solving the MMF capacitated problem is more complicated,
since in general it is not enough to find a flow allocation vector
that maximizes the minimal flow \(X_d\) over all demands \(d\).
Even if such a flow vector \(X\) is found,
then in general some link capacities might still be free and can be used
to increase flow allocations for at least a subset of demands.

\section{Topological Design}

When installing a link in a network,
there is usually a fixed cost that is independent of the capacity of the link
(e.\,g.~cabling cost).
In order to account for this, such an \emph{opening cost}\index{opening cost}
\(\kappa_e\) needs to be modeled in the objective function
(which is to be minimized):
\begin{align}
	\mathbf{F} &= \sum_{e}\xi_e y_e + \sum_{e} \kappa_e u_e
\end{align}
where \(u_e\) is a binary variable indicating
whether link \(e\) is installed or not.

To force the capacity \(y_e\) to be \(0\) whenever the link \(e\) is not installed,
a large additional constant \(\Delta\) together with additional constraints is introduced,
\begin{align}
	\forall e \in \left\{1, \dotsc, E\right\}: y_e &\leq \Delta u_e
\end{align}

\section{Restoration Design}

So far, the network was always considered to be in operational state,
without accounting for link or node failures.
Now, let's assume the following failure model:
\begin{itemize}
	\item Links can be either fully functional or completely failed
	\item No more than one link fails at a time
	\item Failure state \(s\) (\(s \in \left\{1, \dotsc, \cabs{E}\right\}\))
		indicates that \(s\) links have failed
\end{itemize}

To solve the \emph{restoration design problem}\index{restoration design problem}
(RDP)\index{RDP},
additional indexes \(s\) are introduced to the path flow variables \(x_{dps}\),
referring to that particular flow in case of failure state \(s\).
This also requires reformulating the capacity constraints, e.\,g.:
\begin{align}
	s &= 0: & x_{120} + x_{310} &\leq y_1\\
	s &= 1: & x_{121} + x_{311} &\leq 0\\
	s &= 2: & x_{122} + x_{312} &\leq y_1
\end{align}

Additionally, the notation \(\alpha_{es}\) is introduced,
indicating whether link \(e\) is up or not obtaining the following constraints:
\begin{align}
	\forall s \in \left\{0, \dotsc, S\right\} \forall e \in \left\{1, \dotsc, E\right\}:
	\sum_{d}\sum_{p} \delta_{edp}x_{dps} &\leq \alpha_{es}y_e
\end{align}

A robust network can be considerably more expensive than
the cheapest network without failure considerations.

\section{Intra-Domain Traffic Engineering for IP Networks}

In this scenario, intra-domain routing is operated by an ISP
that has control over the network topology, routing algorithm and link weight system.
Due to service level agreements or experience obtained via measurements,
the ISP knows the demands between nodes of their network.
A common objective of intra-domain routing optimization is
to minimize the (average) delay experience by data packets.
Thus, the goal is to minimize the maximum utilization over all links.

A commonly used intra-domain routing protocol is OSPF,
which is based on Dijkstra's algorithm,
which calculates shortest paths based on some weight system \(\mathbf{w}\).
Thus, the goal is to identify a weight system \(\mathbf{w}\)
such that the maximum link utilization of the network is minimized
while satisfying all given demands and staying within capacity constraints.
This results in path flows and total link loads
begin defined with respect to \(\mathbf{w}\),
as these are now induced by the weight system
influencing how OSPF distributes traffic:
\begin{align}
	\forall d \in \left\{1, \dotsc, D\right\}: \sum_{p}x_{dp}\left(\mathbf{w}\right) &= h_d\\
	\forall e \in \left\{1, \dotsc, E\right\}:
	\underline{y}_e\left(\mathbf{w}\right) &= \sum_{d}\sum_{p} \delta_{edp}x_{dp}\left(\mathbf{w}\right) \leq c_e\\
	\shortintertext{
		The maximum \(r\) over all link utilizations can be computed
		and is needed to ensure that all link loads stay below \(c_er\):
	}
	r &= \max\left\{\frac{\underline{y}_e\left(\mathbf{w}\right)}{c_e} \mmid e=1,\dotsc,E\right\}\\
	\forall e \in \left\{1, \dotsc, E\right\}:
	\underline{y_e}\left(\mathbf{w}\right) &=
	\sum_{d}\sum_{p} \delta_{edp} x_{dp}\left(\mathbf{w}\right) \leq c_e r
\end{align}

This leads to the following optimization problem:
\begin{align}
	\min \mathbf{F} &= r\\
	\shortintertext{subject to}
	\forall d \in \left\{1, \dotsc, D\right\}:
	\sum_{p=1}^{P_d} x_{dp}\left(\mathbf{w}\right) &= h_d\\
	\forall e \in \left\{1, \dotsc, E\right\}:
	\underline{y}_e\left(\mathbf{w}\right) =
	\sum_{d=1}^{D}\sum_{p=1}^{P_d} \delta_{edp}x_{dp}\left(\mathbf{w}\right) &\leq c_e r\\
	r &= \max\left\{\frac{\underline{y}_e\left(\mathbf{w}\right)}{c_e} \mmid e=1,\dotsc,E\right\}
\end{align}
With \(r\) being continuous and \(w_e\) being non-negative integers.

For this to work, \(k\) shortest paths
for every attempted weight system vector \(\mathbf{w}\) need to be found.
If \(r^{\ast} < 1\), no link will be overloaded.
However, if \(r^{\ast}\) is close to \(1\),
congestion is likely to occur.
For \(r^{\ast} > 1\) there is at least one overloaded link,
i.\,e.~the demands can not be satisifed appropriately.

\section{Tunnel Optimization for MPLS Networks}

\emph{Multi-Protocol Label Switching}\index{Multi-Protocol Label Switching}
(MPLS)\index{MPLS} is an approach that
introduces virtual connections into packet switched networks
in order to speed up processing times for routers and allow for traffic engineering.
In order to transport traffic in an MPLS network,
a so-called \emph{label switched path}\index{label switched path}
is set up from the source (ingess MPLS node) to the destination (egress MPLS node).
Tunneling (by making use of label stacking) can be used to
handle “similar” traffic in an aggregated way,
allowing for different traffic capabilities like
putting traffic of similar QoS classes into the same tunnels for special treatment
and easy re-routing in case of congestion or link failures.

In order not to overload routers with too many tunnels,
which would increase the processing overhead,
it is desirable to limit the number of tunnels per router and/or link.
Thus the optimization challenge is to carry different traffic classes
in an MPLS network through the creation of tunnels
such that the number of tunnels per node/link is minimized
and the load is balanced amoung routers/links.
For this, the same notation as before can be used,
with \(x_{dp}\) now denoting the fraction of demand
that is routed over path \(P_{dp}\), resulting in the demand constraint:
\begin{align}
	\forall d \in \left\{1, \dotsc, D\right\}:
	\sum_{p=1}^{P_d} x_{dp} &= 1
\end{align}
Note that \(x_{dp} \in \left[0, 1\right]\)
and the absolute flow transported is now \(h_d\cdot x_{dp}\).

To avoid path flows with very low fractions,
a lower bound \(\varepsilon\) is introduced
together with binary variables \(u_{dp}\) indicating
whether the lower bound is satisfied or not:
\begin{align}
	\forall d \in \left\{1, \dotsc, D\right\}
	\forall p \in \left\{1, \dotsc, P_d\right\}:
	\varepsilon u_{dp} &\leq x_{dp}\\
	\forall d \in \left\{1, \dotsc, D\right\}
	\forall p \in \left\{1, \dotsc, P_d\right\}:
	x_{dp} &\leq u_{dp}
\end{align}

Capacity feasibility constraints:
\begin{align}
	\forall e \in \left\{1, \dotsc, E\right\}:
	\sum_{d=1}^{D} h_d \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e\\
	\shortintertext{The number of tunnels on link \(e\) will be:}
	\sum_{d}\sum_{p}\delta_{edp}u_{dp}&
\end{align}

The goal is now to minimize the number \(r\)
representing the maximum number of tunnels over all links:

\begin{align}
	\min \mathbf{F} &= r\\
	\shortintertext{subject to}
	\forall d \in \left\{1, \dotsc, D\right\}:
	\sum_{p=1}^{P_d} x_{dp} &= 1\\
	\forall e \in \left\{1, \dotsc, E\right\}:
	\sum_{d=1}^{D}h_d \sum_{p=1}^{P_d} \delta_{edp}x_{dp} &\leq c_e\\
	\forall d \in \left\{1, \dotsc, D\right\}
	\forall p \in \left\{1, \dotsc, P_d\right\}:
	\varepsilon u_{dp} &\leq x_{dp}\\
	\forall d \in \left\{1, \dotsc, D\right\}
	\forall p \in \left\{1, \dotsc, P_d\right\}:
	x_{dp} &\leq u_{dp}\\
	\forall e \in \left\{1, \dotsc, E\right\}:
	\sum_{d=1}^{D}\sum_{p=1}^{P_d} \delta_{edp}u_{dp} &\leq r
\end{align}
and \(x_{dp}\) continuous and non-negative, \(u_{dp}\) binary and \(r\) integer.

This problem has both continuous and binary variables,
whilch the constraints and objective function are linear.
It is an example for a \emph{mixed-integer linear programming problem} (MIP)
\index{mixed-integer linear programming problem}\index{MIP}.
Finding exact solutions for MIP is more difficult than for
linear optimization problems,
branch-and-bound and banch-and-cut being established techniques
to solve such problems.
