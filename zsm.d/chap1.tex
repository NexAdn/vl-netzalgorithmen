\chapter{Introduction}
\section{Basic Types of Transmissions}

\begin{description}
	\item[Web]
		\begin{itemize}
			\item Bunch of data to be transmitted
			\item No guaranteed arrival times
			\item Simplest Case: Server and Client are directly connected by a cable
		\end{itemize}
	\item[Telephony]
		\begin{itemize}
			\item Continuous flow of information
			\item Information must arrive in time
			\item Simplest Case: Two telephones are directly connected via a cable
		\end{itemize}
\end{description}

\section{Structuring a Network}

As pairwise connection of all entities with each other
(thus building a complete graph of all entities)
does not work, other structures need to be established.
We distinguish between \emph{end systems} (user devices)\index{end system}
and \emph{switching elements}\index{switch}\index{router} (switches, routers, etc.).

\begin{description}
	\item[End systems] connect to some form of uplink to access/provide
		information on the network.
		They do not forward requests of other systems.
	\item[Switching elements] Forward incoming packets onto
		the next hop towards its destination.
		The “best” next hop is decided by various routing/forwarding tables and algorithms.
\end{description}

\section{Routing Algorithms}

Routers execute \emph{routing} algorithms\index{routing} to decide which output line
an in coming packet should be transmitted on.

\begin{description}
	\item[Connection-oriented services]
		Run the routing algorithm during connection setup and only once
		to find one path to forward the packets along
		for the whole lifetime of the connection.
	\item[Connectionless services]
		Run the routing algorithm either for each packet
		or periodically, updating the router's forwarding table in the process.
\end{description}

Routing algorithms can take a \emph{metric} into account
that assigns costs to network links
and allows administrators to influence routing decisions.
Some possible metrics are:
\begin{itemize}
	\item Financial cost for sending a packet over a link
		(e.\,g.~when the link is charged per unit of data transferred).
	\item Delay (useful to penalize using a link with high delay
		when trying to prefer links with low delay)
	\item Number of hops (commonly used in most routing algorithms
		deployed on the Internet,
		aims to reduce the number of routers/networks to traverse to reach a destination)
\end{itemize}
The cheapest path is also commonly referred to as
the \emph{shortest path}\index{shortest path}.

Basic types of routing algorithms:
\begin{description}
	\item[Non-adaptive routing algorithms]
		do not base routing decisions on the current state of the network.
	\item[Adaptive routing algorithms]
		take into account the current network state
		(e.\,g.~distance vector routing, link state routing).
\end{description}

\section{Flooding}

\emph{Flooding}\index{flooding} is a simple strategy,
sending every incoming packet to every outgoing link
except the one it arrived on.
This leads to many duplicated packets in the network,
but leads to the packet almost certainly arriving at the destination
(the only exception being broken links partitioning the network
into two parts).

To reduce the number of duplicated packets,
strategies can be used:
\begin{description}
	\item[Solution 1: Hop counting]
		Have a hop counter in the packet header,
		which is decremented by each router.
		If the packet stays in the network for too long,
		the hop counter goes to 0 and the packet is dropped.
		Ideally, the hop counter should be initialized
		to the length of the shortest path from the source to the destination.

	\item[Solution 2: Sequence numbers]
		Each router maintains a sequence number and
		a table of sequence numbers it has seen from other routers.
		The first-hop router increments and adds its sequence number to
		each incoming packet from a host.
		Each router only forwards incoming packets,
		if it hasn't seen this sequence number from the first-hop router, yet.
		Thus, packets that have already been seen are discarded.
\end{description}

\section{Adaptive Routing Algorithms}\index{adaptive routing}

Non-adaptive routing algorithms pose problems:
\begin{itemize}
	\item Non-adaptive routing algorithms can't cope
		with dramatic changes in traffic levels in different parts of the network.
	\item Non-adaptive routing algorithsm are usually based on average traffic conditions,
		but lots of computer traffic us extremely \emph{bursty}\index{bursty}
		(i.\,e.~very variable in intensity).
\end{itemize}
Thus, adaptive routing algorithms are commonly used to make routing decisions.

Three types can be distinguished:
\begin{description}
	\item[Centralized adaptive routing]
		Has only one central routing controller making routing decisions.
	\item[Isolated adaptive routing]
		Is based on information local to each router.
		No exchange of information between routers is required.
	\item[Distributed adaptive routing]
		Uses periodic exchanges of information between routers
		to compute and update routing information
		to be stored in the local forwarding table.
\end{description}

\subsection{Centralized Adaptive Routing}

At the heart of Centralized Adaptive Routing is a central routing controller,
which
\begin{itemize}
	\item periodically collects link state information from routers
	\item calculates routing tables for each router
	\item dispatches updated routing tables to each router
\end{itemize}

The centralized approach is severely limited by the routing controller.
If it goes down, the routing becomes non-adaptive,
making the network vulnerable to outages.
Furthermore, the controller needs to handle a great deal of routing information,
making it not only a single point of failure,
but also a bottleneck for scalability and performance of the network.

\subsection{Isolated Adaptive Routing}

The basic idea is to make routing decisions solely based on information
available locally in each router, e.\,g.:
\begin{itemize}
	\item \emph{Hot potato}\index{hot potato}
	\item \emph{Backward learning}\index{backward learning}
\end{itemize}

Hot potato routing:
\begin{itemize}
	\item Forward the incoming packet to the output link with the shortest queue
	\item Do not care where the selected output link leads
	\item Not very effective
\end{itemize}

Backward learning:
\begin{itemize}
	\item Maintain a local forwarding table with next hop, hop count and output link
	\item Incoming packets update the fowarding table entry of the sender
		if their hop count is better than the entry's current hop count
	\item Forward packets based on the forwarding table,
		random route (hot potato, flood) the packet if no entry for the destination exists.
	\item Remove/forget stale entries in the forwarding table
		to account for deterioration of routes (e.\,g.~in case of link failures)
\end{itemize}

Ethernet switches commonly use backward learning
to maintain forwarding tables for MAC addresses,
usually falling back to flooding packets if no entry for
the destination MAC exists in their local forwarding table.

\subsection{Distributed Adaptive Routing}

The central goal is to determine a “good” path
(i.\,e.~a sequence of routers) through the network from source to destination.
For calculations,
the network is abstracted into a graph consisting of:
\begin{description}
	\item[Routers] represented by nodes
	\item[Links] represented by edges
	\item[Costs] assigned to edges, representing link costs
\end{description}

Routing algorithms can be classified in several ways:
\begin{itemize}
	\item Global or decentralized information
		\begin{description}
			\item[Decentralized]
				All routers know only a portion of the network,
				i.\,e.~their physically connected neighbors and link costs to their neighbors.
				By exchanging information with neighbors,
				routes to other destinations can be calculated.
				Examples: BGP (path vector), RIP (distance vector)
			\item[Global]
				By exchanging information,
				routers gain knowledge of the full network topology
				and the cost of each link.
				This is used to compute cheap routes to each destination in the network.
				Examples: OSPF, IS-IS
		\end{description}

	\item Static or dynamic routes
		\begin{description}
			\item[Static] Routes change only slowly over time,
				e.\,g.~if they are statically configured by network administrator.
			\item[Dynamic] Routes change more quickly in response to link cost changes
				and require periodic updates of routing information.
		\end{description}
\end{itemize}

\subsection{Graph Model for Routing Algorithms}

\begin{itemize}
	\item \(V = \left\{v_1, v_2, \dotsc, v_n\right\}\) the set of nodes (routers)
	\item \(E = \left\{e_1, e_2, \dotsc, e_m\right\} \subseteq V^2\) the set of edges (links)
	\item \(c: V \times V \to \mathbb{Z}_{>0}\) cost of an edge
	\item \(s \in V\) start node
		(i.\,e.~the node for which the shortest path shall be found)
	\item \(d\left[i\right]\) cost from \(s\) to node \(v_i\)
	\item \(p\left[i\right]\) index \(j\) of the predecessor \(v_j\) of \(v_i\) on
		the shortest path from \(s\) to \(v_i\).
	\item \(\delta\left(s, v\right)\) cost of the shortest path from \(s\) to \(v\)
\end{itemize}

\subsection{Dijkstra's Algorithm for Shortest Paths}\index{Dijkstra}

Maintain a set \(N\) (initially empty) of nodes for which shortest have been found.
For each node \(i\) that is not directly connected to \(s\),
set \(d\left[i\right] = \infty\),
otherwise \(d\left[i\right] = c\left(s, v_i\right)\).
Starting from \(s\),
take \(v_i \in V\setminus N\) with the lowest \(d\left[i\right]\).
The path from \(v_i\)'s predecessor to \(v_i\) is the shortest path.
Update \(d\left[j\right]\) for neighbors \(v_j \in V \setminus N\) of \(v_i\),
wherever \(d\left[i\right] + c\left(v_i, v_j\right) < d\left[j\right]\)
(i.\,e.~the path from \(s\) to \(v_j\) via \(v_i\)
is shorter than the previously known path from \(s\) to \(v_j\)).
Set \(N := N \cup \left\{v_i\right\}\).
This results in finding the shortest paths from \(s\) to each node in the network.

Complexity:
\begin{itemize}
	\item \(\bigO\big(\cabs{V}^2\big)\)
	\item Optimal in dense graphs with \(\cabs{E} \approx \cabs{V}^2\)
	\item Efficient implementations with
		\(\bigO\big(\cabs{V}\cdot \log \cabs{V} + \cabs{E}\big)\)
		are possible when using Fibonacci-Heaps.
\end{itemize}

\subsection{Distance Vector Routing}\index{distance vector}

For distance vector routing,
each node has its own table for \(D^X\left(Y, Z\right)\),
listing the cost from \(X\) to \(Y\) via \(Z\) as next hop.
Distance vector routing has a few favorable properties,
useful in large networks like the Internet:
\begin{description}
	\item[Iterative] The algorithm works iteratively,
		until no nodes exchange information
		(i.\,e.~no updated information is transmitted through the network)
	\item[Asynchronous] Information does not need to be exchanged in lock step.
		Instead any node can send updated information at any time.
	\item[Distributed]
		Each node only needs to communicate with its directly attached neighbors.
\end{description}

Initially, all routers only know the costs to their neighbors
and set the cost to any other destination to \(\infty\)
(aka. unreachable).
Afterwards they notify neighbors of their costs to each destination they know of.
Then, distance vector routing algorithms continuously
wait for changes in local link costs or link update messages from neighbors.
Whenever such a change occurs,
the information is used to update the local distance table.
If any changes to shortest (!) paths have occured,
neighbors are notified of the updated shortest path costs.

The main problem of distance vector routing is the
\emph{count to infinity}\index{count to infinity} problem.
If the link cost for a link suddenly increases (possibly to \(\infty\)),
the network might now have a shortest path to a destination
going in a circle for some time,
while the change in link cost is continuously increased in the network
until the new cost for the link is reached or an alternative, shorter path is found.
This increases the time to find a new shortest path dramatically!
This issue can be mitigated with \emph{poisoned reverse}\index{poisoned reverse},
i.\,e.~when \(Z\) routes to \(X\) via \(Z\)
and \(Y\) notifies \(Z\) that the cost to \(X\) changed,
\(Z\) notifies \(Y\) that its cost to \(X\) is \(\infty\)
to prevent \(Y\) from routing to \(X\) via \(Z\)
(as \(Z\) would want to route to \(X\) via \(Y\),
making the packets go in a loop).

\subsection{The Bellman-Ford Algorithm}

The \emph{Bellman-Ford algorithm}\index{Bellman-Ford algorithm}
is capable of solving the problem
of computing shortest path in graphs with edges with negative costs
and is the basis for distance-vector routing.
Its only limitation is that there must be no cycles with negative total cost,
as otherwise the shortest path's cost will go to \(-\infty\)
(as continuously traversing such a cycle will inifinitely reduce the total cost).
The algorithm can detect if such cycles with negative total cost exist.

The algorithm iteratively improves the estimated cost to reach a node
by iterating \(\cabs{V} - 1\) times over all edges and check
if the current estimate of the node
can be improved by taking any of the connected edges,
given the current estimate cost.
As this algorithm always checks all edges,
it has a higher running time of \(\bigO\big(\cabs{V} \cdot \cabs{E}\big)\).

Negative cost cycles can be detected by running the algorithm \(\cabs{V}\) times.
If the cost to any node has changed in the \(\cabs{V}\)th iteration,
there must be a negative cost cycle.

\subsection{Comparison of Link State and Distance Vector Algorithms}

\begin{description}
	\item[Message complexity] How many messages are exchanged?
		\begin{itemize}
			\item Link State: with \(n\) nodes, \(E\) links,
				\(\bigO\left(n \cdot E\right)\) messages are sent by each node
			\item Distance Vector: exchange only between neighbors,
				but variable number of messages and variable convergence time
		\end{itemize}

	\item[Speed of Convergence] How long does it take until the routing table doesn't change
		after a link state change has occured?
		\begin{itemize}
			\item Link State: \(\bigO\left(n^2\right)\) algorithm requires
				\(\bigO\left(n \cdot E\right)\) messages
			\item Distance Vector: variable convergence time,
				in part caused by routing loops and the count-to-infinity problem
		\end{itemize}

	\item[Robustness] What happens if a router malfunctions?
		\begin{itemize}
			\item Link State:
				\begin{itemize}
					\item Node can advertise incorrect link cost
					\item Invalid routing table calculations only affect the malfunctioning router
				\end{itemize}

			\item Distance Vector:
				\begin{itemize}
					\item Nodes can advertise incorrect path costs
					\item Each node's table is used (in part) by other routers,
						so errors can propagate through the network
				\end{itemize}
		\end{itemize}
\end{description}

\section{Hierarchical Routing and Interconnected Networks}

So far, an idealized scenario with identical routers and a “flat” network was assumed.
In practice, networks scale to hundreds millions of destinations,
making storing detailed routing tables for all destinations in the whole network
technically impossible,
due to memory limitations in routers and link overloads
caused by routing table exchanges between routers.
Furthermore, administrative autonomy in parts of the network
(like in the Internet's autonomous systems) should allow for network administrators
to control the routing (especially the routing protocol in use) in their network,
independent of the rest of the network.

In \emph{interconnected networks}\index{interconnected network},
data transmission usually involves multiple networks.
Routing can be distinguished into two levels:
\begin{description}
	\item[Intradomain routing]\index{intradomain routing} inside autonomouse systems.
	\item[Interdomain routing]\index{interdomain routing} between autonomous systems
\end{description}
In the Internet, interdomain routing is done using the Border Gateway Protocol (BGP),
which operates on the AS level and considers every AS as one hop.
For intradomain routing, each network administrator can choose their AS's
interior routing protocol (e.\,g.~OSPF, RIP, iBGP, IS-IS).

For sending traffic between ASes,
\emph{Internet Service Providers}\index{Internet Service Provider}\index{ISP} (ISPs)
have peering agreements and connections with and to each other,
making a data transfer possible.
Depending on the policies and available links,
traffic may not be able to be sent directly from the source ISP to the destination ISP,
but needs to be sent to a different transport provider network first
(transit\index{transit}).

Each network operator has to make decisions regarding
how to handle the traffic in their network, including
\begin{itemize}
	\item allocating enough capacity of routers and links,
	\item choosing a routing algorithm,
	\item setting link costs.
\end{itemize}
This requires estimation of \emph{traffic demand}\index{traffic demand}\index{demand}
in the network.
This can be displayed as \emph{demand volume matrix}\index{demand volume matrix}
\(H: \left\{1, \dotsc, n\right\}^2 \to \mathbb{N}\),
denoting the traffic demand volume between nodes \(v_i\) and \(v_j\) as \(H\left[i,j\right]\),
also abbreviated \(h_{ij}\) later on.

\section{Considerations on Traffic Demand and Link Utilization}

To understand constraints on maximum link utilization,
a few basic facts about the natur of Internet traffic need to be recapitulated:
\begin{itemize}
	\item Packets are delayed in every router
		due to store-and-forward processing and queueing.
	\item Traffic congestion can occur in parts of the Internet.
	\item Packets may be dropped if arriving at a router with full output queues.
\end{itemize}
The task of a network designer is to design the network such that
delay, congestion and the probability of packet drops are minimized,
while also allowing for a reasonable utilization of the network.
This is complicated by the fact that
traffic arrival patterns and packet sizes in the Internet are random.
In order to characterize Internet traffic behavior,
large scale measurements are needed to gain insights about
traffic arrival distribution and packet size distribution.

\paragraph*{Important observation:}
Internet traffic does not follow commonly known distributions
like normal or exponential distributions,
but shows self-similar characteristics and can have
\emph{heavy-tailed}\index{heavy-tailed} distributions,
i.\,e.~distributions with high skewness.

For simplicity, a few assumptions are made:
\begin{itemize}
	\item Packets arrive according to a Poisson process with rate \(\lambda\):
		\begin{align}
			P_n\left(t\right) & = \frac{\left(\lambda t\right)^n}{n!} e^{-\lambda t}
		\end{align}
		(on average, one arrival in every time interval of length \(\frac{1}{\lambda}\)).
	\item Packet size is exponentially distributed,
		leading to exponentially distributed service times with rate \(\mu\)
\end{itemize}
Considering only one router, such a system can be thought of
as an M/M/1 queueing system.

\section{The Poisson Process}

Let \(A\left(t\right)\) (\(t \geq 0\)) be the number of packets arriving
in \(\left(0, t\right]\).
Requirements:
\begin{itemize}
	\item \(A\left(0\right) = 0\)
	\item Idependence of teh number of arrivals in disjoint time periods
	\item Singularity of arrival events (packets never arrive in parallel)
	\item Stationary process of arrivals:
		the probability how many arrivals happen in a time interval
		only depends on the interval length.
\end{itemize}

Denote the probablity that \(n\) packets arrive in \(\left(0, t\right]\) as follows:
\begin{align}
	P_n\left(t\right) &:= \mathrm{Pr}\left[A\left(t\right) = n\right]
\end{align}
Due to the aformentioned requirements, we have
\begin{align}
	P_0\left(0\right) &= 1 & \forall n > 0 : P_n\left(0\right) &= 0
\end{align}

After a bunch of math that no one in their right mind can memorize, we obtain:
\begin{align}
	P_n\left(t\right) &= \frac{\left(\lambda t\right)^n}{n!} e^{-\lambda t}
\end{align}

\section{Little's Law}

Let \(\mathrm{Arrival}\left(T\right)\) be the number of packets arrived until time \(T\),
\(W_i\left(T\right)\) be the waiting time of packet \(i\) at time \(T\),
\(N\left(T\right)\) be the number of packets in the system at time \(T\).

We are interested in the accumulated (total) waiting time of all jobs
that ever arrived in the system until time \(T\).
This can be computed either as the sum of waiting times of all packets arrived
until time \(T\)
or as the integral over the number of packets in the system during \(\left(0, T\right]\).
Both methods lead to the same result.

Now, let \(\lambda\left(T\right)\) be
the average number of packets in \(\left(0, T\right]\),
\(\overline{W}\left(T\right)\) be the average waiting time of a packet
and \(\overline{N}\left(T\right)\) be the average number of packets in the system.
Then we get \emph{Little's Law}\index{LittlesLaw@Little's Law}:
\begin{align}
	\lambda \cdot \overline{W} &= \overline{N}
\end{align}

\section{M/M/1 System}

The number of packets in the system (queue size) at discrete points in time \(\delta\)
can be described as a \emph{Markov chain}\index{Markov chain},
with the probability of the queue size increasing being \(\lambda\delta\)
and the probability of the queue size decreasing being \(\mu\delta\).
Let \(p_n\) denote the probability of the system having queue size \(n\).
We obtain
\begin{align}
	p_n &= \left(1-\rho\right) \rho^n & \rho &= \frac{\lambda}{\mu}\\
	\overline{N} &= \frac{\lambda}{\mu - \lambda}\\
	\overline{W} &= \frac{1}{\mu - \lambda}
\end{align}
Thus, with \(\rho \to 1\) (i.\,e.~the system load is so high
that on average each packet takes as long to process
as new packets arrive on average),
the average waiting time and queue size go to infinity.
Since in reality the queue size is limited, this will lead to packets being dropped.

If packets have average size \(K_p\) bits and link capacity is \(C\) bits per second,
then the average service rate of the link is
\begin{align}
	\mu_p &= \frac{C}{K_p} \text{ pps (packets per second)}\\
	\shortintertext{
		If the average arrival rate is \(\lambda_p\) pps,
		then the average delay is given by
	}
	D\left(\lambda_p, \mu_p\right) &= \frac{1}{\mu_p - \lambda_p}
\end{align}

This leads to an important insight: To maintain low delays, 
link utilization should be kept low, e.\,g.~below \(50\,\%\).
Thus, when link utilization reaches a certain threshold,
it should be upgraded.
From a delay perspective, it's better to have one high bandwidth link
than multiple lower bandwidth links.
This is often referred to as the
\emph{statistical multiplexing gain}\index{statistical multiplexing gain}.

Contrary to this, fault tolerance may call for having multiple links.
Also, on a single link, misbehaving traffic flows are difficult to control.

\section{Notion of Routing and Flows}

Routing can not only be interpreted as the decision
how an individual packet may be transported in the networks,
but also how ensemble traffic may be routed between the same two points
(e.\,g. points of presence, data centers).
For the remainder of the lecture, this second notion is used
and instead of making routing decisions for individual packets,
decisions for whole flows are made.
Routing decisions then need to stay withing capacity constraints
or can influence capacity decisions.

\section{Multi-Level Networks}

When doing interconnects over transit providers,
the network architecture can be viewed both in the \emph{transport view}
(i.\,e.~the (underlay) network of the transport provider)
as well as the \emph{traffic view}
(i.\,e.~the flow of traffic between nodes in the network).

Links in the traffic network are \emph{logical links}\index{link!logical}
and must be mapped to links/paths in the transport network.
The mapping can change the properties of the network from one view to the other.
There can be logical links between nodes in the traffic view
for nodes that are not physically connected in the transport view,
e.\,g.~if traffic between these nodes needs to be transported over a few switches.
