\chapter{Network Resilience}

\section{Introduction}

\emph{Network resilience}\index{network resilience} denote the property of a network
to sustain the ability to communicate
even if parts (nodes, links) of the network fail.
Quantification of random failures often computes the probability of certain conditions,
e.\,g.~partitioning of a network, etc.
Quantification of failures due to deliberate attacks often computes
the worst case damage, e.\,g.~smallest number of attacked/failed links or nodes
so that the remaining network is partitioned, etc.
Additionally, the smallest number of additional links that need to be added
in order to increase the resilience of a network against random failures
or deliberate attacks can be of interest.

Definitions from Graph Theory:
\begin{itemize}
	\item Two paths \(p_1\) and \(p_2\) from \(x\) to \(y\) in \(G\)
		are \emph{edge independent}\index{edge independent},
		if they have no link in common.
	\item Two paths \(p_1\) and \(p_2\) from \(x\) to \(y\) in \(G\)
		are \emph{node independent}\index{node independent},
		if they only have nodes \(x\) and \(y\) in commong.
	\item If there is at least one path linking every pair of actors in the graph
		then the graph is called \emph{connected}\index{connected}.
	\item If there are \(k\)-edge-independent paths connecting every pair,
		the graph is \emph{\(k\)-edge-connected}\index{k-edge-connected@\(k\)-edge-connected}.
	\item If there are \(k\) node-independent paths connecting every pair,
		the graph is \emph{\(k\)-node-connected}\index{k-node-connected@\(k\)-node-connected}.
	\item The biggest number \(k\) for which \(G\) is \(k\)-edge-connected
		is called the \emph{edge-connectivity}\index{edge-connectivity} of \(G\).
	\item The biggest number \(k\) for which \(G\) is \(k\)-node-connected
		is called the \emph{node-connectivity}\index{node-connectivity} of \(G\).
		Graphs that are 2-node-connected are also called \emph{biconnected}\index{biconnected}.
	\item In any connected component,
		the path(s) linking two non-adjacent nodes must pass through a subset of other nodes,
		which if removed, would disconnect them.
		\begin{itemize}
			\item For two nodes \(s\) and \(t\) the set \(T \subseteq V \ \left\{s, t\right\}\)
				is called an
				\emph{\(s\)-\(t\)-cutting-node-set}\index{s-t-cutting-node-set@\(s\)-\(t\)-cutting-node-set}
				if every path connecting \(s\) and \(t\) passes through at least one node of \(T\),
				that is there is no path from \(s\) to \(t\) in \(G \setminus T\).

			\item A set \(T\) is called a \emph{cutting-node-set}\index{cutting-node-set}
				if \(T\) is an \(s\)-\(t\)-cutting-node-set for two nodes \(s\) and \(t\).

			\item For two nodes \(s\) and \(t\) the set \(F \subseteq E\)
				is called an
				\emph{\(s\)-\(t\)-cutting-edge-set}\index{s-t-cutting-edge-set@\(s\)-\(t\)-cutting-edge-set}
				if every path connecting \(s\) and \(t\) traverses at least one edge of \(F\),
				that is there is no path from \(s\) to \(t\) in \(G \setminus F\).

			\item A set \(F\) is called a \emph{cutting-edge-set}\index{cutting-edge-set}
				if \(F\) is an \(s\)-\(t\)-cutting-edge-set for two nodes \(s\) and \(t\).
		\end{itemize}
\end{itemize}

\section{Menger's and Whitney's Theorems}

\paragraph*{Menger's Theorem:}\index{Mengers Theorem@Menger's Theorem}
For non-adjacent nodes \(s\) and \(t\) in an undirected graph,
the maximum number of node independent paths is equal to
the minimum size of \(s\)-\(t\)-cutting-node-set.
For nodes \(s\) and \(t\) in an undirected graph,
the maximum number of edge independent paths is equal to
the minimum size of an \(s\)-\(t\)-cutting-edge-set.

\paragraph*{Whitney's Theorem:}\index{Whitneys Theorem@Whitney's Theorem}
An undirected graph with at least \(k+1\) nodes
is \(k\)-node-connected iff each cutting-node-set in \(G\)
contains at least \(k\) nodes.
An undirected graph is \(k\)-edge-connected iff each cutting-edge-set in \(G\)
contains at least \(k\) edges.

\paragraph*{Implications for communication networks:}
\begin{itemize}
	\item If a communication network is supposed to allow communication between arbitrary nodes
		even in case of failure of \(r\) arbitrary nodes,
		its topology must be at least \(\left(r+1\right)\)-node-connected.

	\item If a communication network is supposed to allow communication betwee arbitrary nodes
		even in case of failure of \(s\) arbitrary links,
		its topology must be at least \(\left(s+1\right)\)-edge-connected.
\end{itemize}

Thus, the network graph can be analyzed to get information about resilience
by calculating the highest \(k\) for which \(G\) is \(k\)-edge/node-connected.
This can be solved in polynomial time.

If a graph is not \(k\)-edge/node-connected augmentations to \(G\) can be searched
to find a minimum set of edges/nodes to add
so that \(G\) becomes \(k\)-edge/node-connected.
For edge-connectivity, this can be solved in polynomial time.
For node-connectivity, polynomial algorithms are only known for
\(k \leq 4\).
The weighted variant with the objective of weight minimization
is NP-hard already for \(k=2\).

\section{Block Structure of Graphs}

Let \(G = \left(V, E\right)\) be an undirected Graph with \(\cabs{V} \geq 3\).
Of interest are all subgraphs of maximum sizie that are biconnected.
\(G\) is biconnected iff either
\(G\) is a single edge or for each tuple of vertices \(\left(u, v\right)\)
there are at least to node disjoint paths.
The intersection of two maximum size biconnected components
consists of at most one vertex,
which is called \emph{articulation node}\index{articulation node}.
A graph \(G\) with at least 3 nodes is biconnected,
iff \(G\) does not contain isolated nodes
and every pair of nodes is on a common single cycle.

Let \(e_1, e_2 \in E\) and \(\equiv\) be defined
as \(e_1 \equiv e_2\) iff \(e_1\) and \(e_2\) lie on a common simple cycle.
This equivalence relation partitions \(E\) into sets \(E_1, E_2, \dotsc, E_h\).
Let \(G_i = G\left[E_i\right]\) for \(i \in \left\{1, \dotsc, h\right\}\).
These subgraphs are called \emph{blocks}\index{block}.
Blocks with at least 2 edges are the maximum sized biconnected components of \(G\).

Let \(G_i = G\left[E_i\right] = \left(V_i, E_i\right)\) be the blocks of \(G\), then
\begin{itemize}
	\item \(\forall i \neq j \in \left\{1, \dotsc, h\right\}: \cabs{V_i \cap V_j} \leq 1\).
	\item A node \(a \in V\) is an articulation node iff
		\(\exists i \neq j \in \left\{1, \dotsc, h\right\}: V_i \cap V_j = \left\{a\right\}\).
\end{itemize}

The block structure of a graph can be describe with a so-called
\emph{block structure graph}\index{block structure graph} \(B\left(G\right)\)
that contains nodes \(v_a\) for each articulation node \(a\)
and nodes \(v_b\) for each block \(b\) with each \(v_a\) being connected to
the nodes \(v_b\) denoting the respective biconnected components \(b\) that
node \(a\) is connected to in \(G\).
\(B\left(G\right)\) is a tree.

Calculating \(B\left(G\right)\) allows to identify articulation nodes
as well as the blocks of a graph.
It also allows network designers to see
which nodes in the network are more important to protect
or between which components of the network additional links are needed.

\section{DFS Spanning Trees on Network Graphs}

\subsection{Classification of Edges}

Articulation nodes can be found in \(\bigO\left(\cabs{V} \left(\cabs{V} + \cabs{E}\right)\right)\),
by deleting each node and using DFS to calculate a spanning tree
to see if it's still connected.
During DFS, \emph{preorder}\index{preorder} and \emph{postorder} numbers can be calculated,
which are incremented for each node before and after recursion, respectively.

Classification of edges of \(G\) with respect to a spanning tree \(T\):
\begin{itemize}
	\item An edge \(vw \in E\left(T\right)\) is called a \emph{tree edge}\index{tree edge}.
	\item An edge \(vw \in E\left(G\right) \setminus E\left(T\right)\) is called a
		\emph{back edge}\index{back edge}
		if \(v\) is a descendant or ancestor of \(w\).
	\item Else, \(vw\) is called a \emph{cross edge}\index{cross edge}
		(these don't exist if \(T\) is a depth-first-search tree).
\end{itemize}

Let \(D_v\) be the number of descendants of \(v\).
Then \(w\) is descendant of \(v\) iff
\(v.pre < w.pre \leq v.pre + D_v\).
The following notation is used for edges
with respect to the DFS-tree \(T\):
\begin{itemize}
	\item \(u \treedge v\) iff \(uv \in E\left(T\right)\)
	\item \(u \ancestor v\) iff \(u\) is an ancestor of \(v\)
	\item \(w \backedge u\) iff \(wu\) is back edge
		if \(wu\) in \(E\left(G\right) \setminus E\left(T\right)\) with either
		\(w \ancestor u\) or \(u \ancestor w\).
\end{itemize}

\subsection{Computing Articulation Nodes}

If \(w\) is descendant of \(v\) and \(wu\) is back edge such that \(u.pre < v.pre\),
then \(u\) is a proper ancestor of \(v\).
In a DFS tree \(T\), a node \(v\) other than the root is an articuation node iff
\(v\) is not a leaf and some subtree of \(v\) has
no back edge incident to a proper ancestor of \(v\) and
some subtree of \(v\) has no back edge incident to a proper ancestor of \(v\).

To efficiently implement this test, the so-called \emph{low value}\index{low value}
is used:
For each vertex \(v\) define
\begin{align}
	low\left(v\right) &:= \min\left(
		\left\{v.pre\right\} \cup \left\{w.pre \mmid v \ancestor \backedge w\right\}
	\right)
\end{align}
With \(v \ancestor\backedge w\) meaning that \(v\) is connect to \(w\)
through a path of tree edges and potentially one additional back edge
as the last edge.
\begin{align}
	low\left(v\right) &= \min\left(
		\left\{v.pre\right\} \cup \left\{low\left(w\right) \mmid v \treedge w\right\}
		\cup \left\{w.pre \mmid v \backedge w\right\}
	\right)
\end{align}

\(low\left(v\right) = v.low\) can be computed for all nodes \(v \in V\left(G\right)\)
by using DFS and evaluationg preorder values of incident nodes as each node is visited.
For each node \(v\) that is visited during DFS, set \(v.pre\),
initialize \(v.low := v.pre\) and consider all edges of \(v\):
\begin{itemize}
	\item For tree edges to unvisited nodes \(w\),
		perform a recursive call and after it returns
		and \(w.low\) has been computed properly,
		set \(v.low := \min\left\{v.low, w.low\right\}\).

	\item For back edges to nodes \(w\) that have already been visited,
		set \(v.low := \min\left\{v.low, w.pre\right\}\).
\end{itemize}

A node \(a\) is an articulation node iff either
the node \(a\) is the DFS tree root with \(\geq2\) tree children
or the node \(a\) is not the DFS tree root,
but it has a tree child \(v\) with \(low\left(v\right) \geq a.pre\).
Thus, the articulation nodes of a graph can be calculated with
a slightly modified DFS in \(\bigO\left(\cabs{V} + \cabs{E}\right)\).

\subsection{Computing the Blocks of a Graph}

In order to also compute the blocks of \(G\)
while computing articulation points,
an additional stack \(s\) of edges is introduced:
\begin{itemize}
	\item Whenever a tree edge \(v \to w\) is found,
		push it to \(s\) prior to making the recursive call.
	\item Whenever a back edge is found, push it to \(s\).
	\item Whenever a recursive call for node \(w\) returns to \(v\)
		and \(w.low \geq v.pre\), then
		all edges on top of the stack up to \(v \to w\)
		form the next identified block.
\end{itemize}
With all operations for the stack being done in \(\bigO\left(\cabs{V} + \cabs{E}\right)\)
(holds true when memory is preallocated),
the running time of the algorithm is \(\bigO\left(\cabs{V} + \cabs{E}\right)\).
