\documentclass{vorlesung}

\title{Netzalgorithmen}
\author{Prof.~Dr.-Ing~Günter Schäfer}

\usepackage{booktabs}
\usepackage{todonotes}
\usepackage{gauss}

\newcommand*{\demand}[1]{\left\langle#1\right\rangle}

\begin{document}
\vorlesunghead

\chapter{Einführung}

\chapter{Modeling Network Design Problems}

\section{Modeling Design Problems in Link-Path Formulation}

Preliminary notation for undirected demands\index{demand!undirected}:
\begin{itemize}
	\item \(x-y\)\quad\ldots Link\index{Link!undirected} between node \(x\) and \(y\)
	\item \(\hat{h}_{xy} = b\)\quad\ldots Demand \(d\) between nodes \(x\) and \(y\)
		(i.\,e. Demand \(b\) bandwidth between nodes \(x\) and \(y\))
	\item \(\hat{x}_{v_1v_2} = f\)\quad\ldots Flow\index{flow} on the path between \(v_1\) and \(v_2\)
	\item \(\hat{x}_{v_1v_2v_3} = f\)\quad\ldots Flow on the path \(v_1\), \(v_2\), \(v_3\)
	\item \(\hat{c}_{v_1v_2}\)\quad\ldots Capacity\index{capacity} of the link between \(v_1\) and \(v_2\)
\end{itemize}

This generally yields equations for satisfying the demands and
inequalities for limiting the bandwidth to the link capacities,
e.\,g.

\begin{align}
	\hat{x}_{12} + \hat{x}_{132} &= 5\\
	\hat{x}_{13} + \hat{x}_{123} &= 7\\
	&\vdots\\
	\hat{x}_{132} + \hat{x}_{13} + \hat{x}_{213} &\leq 10\\
	\hat{x}_{132} + \hat{x}_{123} + \hat{x}_{23} &\leq 15
\end{align}

As such a system usually has multiple solutions,
an \cdefname{objective function}\index{objective} is defined,
which can then be optimized, e.\,g.:
\begin{align}
	F &= \hat{x}_{12} + 2\hat{x}_{132} + \hat{x}_{13} + 2\hat{x}_{123} + \hat{x}_{23} + 2\hat{x}_{213}
\end{align}
In this example, flow routed over two links are weighted with factor two,
so direct links are preferred.

Such an optimization is called a \cdefname{multi-commodity flow problem}\index{multi-commodity flow problem}.
The \emph{optimal solution} to this problem is marked with an asterisk, e.\,g.:
\begin{align}
	\hat{x}^{\ast}_{12} &= 5 &
	\hat{x}^{\ast}_{13} &= 7 &
	\hat{x}^{\ast}_{23} &= 8
\end{align}

Important observations:
\begin{itemize}
	\item Changing the objective function usually affects the optimal solution to a problem.
	\item Formulation a good objective function for the particular network
		is important for obtaining meaningful solutions.
\end{itemize}

\section{Modeling Design Problems in Node-Link Formulation}

In this section, links and demands are assumed to be directed.
Undirected links \(x-y\) are replaced with two directed links \(x\to y\) and \(y\to x\).

Notation:
\begin{itemize}
	\item \(v_1\to v_2\)\quad\ldots Directed link\index{link!directed} from node \(v_1\) to node \(v_2\)
	\item \(\demand{v_1:v_2}\)\quad\ldots Demand\index{demand!directed} from node \(v_1\) to node \(v_2\)
	\item \(\tilde{x}_{a,d}\)\quad\ldots Flow\index{flow} over arc \(a\) for demand \(d\)
		(e.\,g. \(\tilde{x}_{v_1v_2,v_1v_2}\) flow over arc \(v_1\to v_2\) for demand \(\demand{v_1:v_2}\))
\end{itemize}
Backflows (e.\,g. \(\tilde{x}_{21,12}\)) are also possible,
but make practically no sense, so they are set to \(0\).

\section{Link-Demand-Path-Identifier-Based Notation}

Demands and links are assigned label indexes.
Thus, tables for mapping indexes to actual demands and links are required.

Notation:
\begin{itemize}
	\item \(h_i\)\quad\ldots Demand\index{demand} with index \(i\)
	\item \(c_e\)\quad\ldots (Known) capacity\index{capacity} of link \(e\)
	\item \(y_e\)\quad\ldots \emph{Unknown} capacity\index{capacity} of link \(e\)
	\item \(P_i\)\quad\ldots Number of candidate paths\index{path!candidate} for demand \(i\)
	\item \(P_{ij}\)\quad\ldots \(j\)th candidate path for demand \(i\)
	\item \(\left(v_1, e_1, v_2, e_2, \dotsc, e_n, v_{n+1}\right)\)\quad\ldots
		\(n\)-hop path\index{path!\(n\)-hop}
	\item \(v_1\to v_2\to \dotsc\to v_{n+1}\)\quad\ldots node representation of a path (directed, use \(-\) instead of \(\to\) for undirected)
	\item \(\left\{e_1, e_2, \dotsc, e_n\right\}\)\quad\ldots link representation of undirected paths
	\item \(\left(e_1, e_2, \dotsc, e_n\right)\)\quad\ldots link representation of directed paths
	\item \(v\)\quad\ldots Node\index{node}
	\item \(e\)\quad\ldots Link\index{link}
	\item \(d\)\quad\ldots Demand\index{demand}
	\item \(p\)\quad\ldots Path\index{path}
	\item \(V\), \(E\), \(D\), \(P\)\quad\ldots total numbers of the aforementioned items
	\item \(\xi_{e}\)\quad\ldots Cost\index{cost} of alink \(e\)
\end{itemize}

Example equations:
\begin{align}
	x_{11} &= 15\\
	x_{21} + x_{22} &= 20\\
	x_{31} + x_{32} &= 10
\end{align}

Demands:
\begin{align}
	\sum_{p=1}^{P_d} x_{dp} &= h_d \qquad d = 1, \dotsc, D\\
	\shortintertext{Short form, when iterating over all candidate paths:}
	\sum_{p} x_{dp} &= h_d \qquad d = 1, \dotsc, D
\end{align}

The vector of all flows (path flow variables) is called the
\cdefname{flow allocation vector}\index{flow allocation vector}
or short \cdefname{flow vector}\index{flow vector}:
\begin{align}
	\mathbf{x} &= \left(\mathbf{x}_1, \mathbf{x}_2, \dotsc, \mathbf{x}_D\right)\\
	&= \left(x_{11}, x_{12}, \dotsc, x_{1P_1}, \dotsc,
	x_{D1}, x_{D2}, \dotsc, x_{DP_D}\right)\\
	&= \left(x_{dp} : d = 1, 2, \dotsc, D; p = 1, 2, \dotsc, P_d\right)
\end{align}
Important: vectors are represented with bold letters \(\mathbf{x}\) and
scalar values are represented with normal letters \(x\).

The relationship between links and paths is written down with
the \cdefname{link-path incidence relation}\index{link-path incidence relation}
\(\delta_{edp}\):
\begin{align}
	\delta_{edp} &= \begin{cases}
		1 & \text{if link } e \text{ belongs to path } p \text{ for demand } d\\
		0 & \text{otherwise}
	\end{cases}
\end{align}
\(\delta_{edp}\) can be written as a table:

\begin{tabulary}{\columnwidth}{CCCC}
	\toprule
	\(e\) &
	\(P_{11} = \left\{2,4\right\}\) &
	\(P_{21} = \left\{5\right\}\) &
	\(P_{22} = \left\{3,4\right\}\)\\
	\midrule
	1 & 0 & 0 & 0\\
	2 & 1 & 0 & 0\\
	3 & 0 & 0 & 1\\
	4 & 1 & 0 & 1\\
	5 & 0 & 1 & 0\\
	\bottomrule
\end{tabulary}

The \cdefname{load}\index{load} in link \(e\) can be written as:
\begin{align}
	\underline{y}_e &= \underline{y}_e\left(\mathbf{x}\right)
	= \sum_{d=1}^{D} \sum_{p=1}^{P_d} \delta_{edp}x_{dp}
\end{align}
\emph{Important:} The actual link loads \(\underline{y}_e\) are determined by 
the path flow variables \(x_{dp}\) of a solution and are not the same as
the link capacity variables \(y_e\).

Cost of a path \(P_{dp}\):
\begin{align}
	\zeta_{dp} &= \sum_{e} \delta_{edp}\xi_{e},\qquad d = 1,2,\dotsc, D\quad p = 1,2,\dotsc, P_d
\end{align}

\paragraph*{Shortest-Path Allocation Rule for Dimensioning Problems}
For each demand, allocate its entire demand to its shortest path
with respect to link costs and candidate paths.
If there is more than one shortest path for a given demand,
then the demand volume can be arbitrarily split amon the shortest paths.

\section{Shortest-Path Routing}

For shorted-path routing,
demands will only be routed on their shortest paths.
The path length is determined by adding up link costs \(w_e\)\index{cost}
according to some weight system \(\mathbf{w} = \left(w_1, w_2, \dotsc, w_E\right)\).

\paragraph*{Single Shortest Path Allocation Problem}
For given link capacities \(\mathbf{c}\) and demand volumes \(\mathbf{h}\),
find a link weight system \(\mathbf{w}\) such that the resulting shortest paths
are unique and the resulting flow allocation vector is feasible.

\todo{Zu\-sam\-men\-fas\-sung fort\-führ\-en}

\chapter{General Optimization Methods for Network Design}

\section{Extreme Points and Basic Solutions}

Eine Lösung heißt Basislösung,
wenn es so viele linear unabhängige Spalten gibt,
wie die Matrix insgesamt an Zeilen hat.

\subsection{Phase 2}

\begin{align}
	S\left(x\right) \text{ linear unabhängig, wenn }
	\sum_{i=1}^{k} \alpha_i \vec{x}_i = 0
	\implies \forall i \in \left\{1, \dotsc, k\right\}: \alpha_i = \vec{o}
\end{align}

\section{The Simplex-Algorithm}

Note: Solving the system of equations means
transforming the target vector into the basis of the input vector.

If all \(d_i\) are non-negative, the basic solution ist the optimal solution
as the \(d_iy_i\) are always subtracted and \(y_i\) is always non-negative,
thus positive \(d_i\) can only decrease the result and not increase it.

\(\vec{z}_k\) is non-negative for all three cases
as \(t_{k,j}\) is non-positive and \(\alpha > 0\).
For items that are not \(j\) and not part of the base,
the result is \(0\) and thus it can be removed from the sum.

If we find that all \(d_j\) are negative, the objective function is unbounded from above.

In case 3, \(\varepsilon\) is always positive due to the way it is defined
and the constrains on the values in this case.
As \(\varepsilon\) is the minimum of the given set,
it can not be \(<0\).

Thus, the resulting vector \(\vec{z}\) can be in the feasible set.

Basically, we have walked from one corner to the next and restarted the algorithm.
In each calculation, we display \(\vec{b}\) in another basis.
Thus, we need to watch out for cycling between multiple results.
This is what Bland's rule is used for.

\subsection{Phase 1}

For distinction, a new variable vector \(\vec{y}\) is introduced.
This is later transformed by extending \(\vec{x}\).
All \(y_i\) are weighted \(1\).

This auxiliary problem can be solved by setting \(\vec{x} = \vec{o}\).

Note that the auxiliary problem is to minimize, not maximize the result.
As all \(y_i\) need to remain positive,
the algorithm will approach \(\forall i: y_i = 0\) eventually.

\subsection{Simplex Tableaus}

Page 35 last line: \(\vec{\hat{b}}\) is actually \(\vec{b}\)\ldots

\paragraph{Example 1:}
\begin{align}
	\max x_1 + x_2 &\\
	\shortintertext{such that:}
	-x_1 + x_2 &\leq 1\\
	x_1 &\leq 3\\
	x_2 &\leq 2\\
	x_1, x_2 &\geq 0
\end{align}
This needs to be transformed from canonical form into the standard form.
For this, additional variables \(x_3, x_4, \dotsc\) need to be introduced:

\begin{align}
	-x_1 + x_2 + x_3 &= 1\\
	x_1 + x_4 &= 3\\
	x_2 + x_5 &= 2\\
	\vec{x} &\geq \vec{o}
\end{align}

A basic solution can be obtained directly:
\begin{align}
	x_1 &= 0\\
	x_2 &= 0\\
	x_3 &= 1\\
	x_4 &= 3\\
	x_5 &= 2
\end{align}
From this, the current maximum value is \(z = 0\).

We now work with the simplex tableau\index{simplex tableau}.

\begin{align}
	\begin{matrix}
		x_1 & x_2 & x_3 & x_4 & x_5 & b\\
		 -1 &   1 &   1 &   0 &   0 & 1\\
		  1 &   0 &   0 &   1 &   0 & 3\\
			0 &   1 &   0 &   0 &   1 & 2\\
			1 &   1 &   0 &   0 &   0 & 0
	\end{matrix}
\end{align}
The last row represents the objective function and the last cell if the last row is \(z\).

This matrix can now be transformed using Gauss-Jordan elimination.
During elimination, we need to ensure that \(\vec{b}\) remains positive!

We apply Blant's rule and find the first column where the value in the last row
is non-zero.
Then, we try to set this cell to \(0\) using the \textsc{Gauss-Jordan} transformation.
Additionally, we transform the matrix to get a unity matrix in the first \(3\) columns.
We continue until we have a unity matrix in the first \(3\) columns
(or can transform the system of equations into such a form by changing the order of rows).

Every time a value in the last row is non-zero (except for \(-z\), of course),
the result can be improved further!

\begin{align}
	&\begin{gmatrix}[v]
		 -1 &   1 &   1 &   0 &   0 & 1\\
		  1 &   0 &   0 &   1 &   0 & 3\\
			0 &   1 &   0 &   0 &   1 & 2\\
			1 &   1 &   0 &   0 &   0 & 0
		\rowops
			\add[1]{1}{0}
			\add[-1]{1}{3}
	\end{gmatrix}\\
	\implies &\begin{gmatrix}[v]
		  0 &   1 &   1 &   1 &   0 &  4\\
		  1 &   0 &   0 &   1 &   0 &  3\\
			0 &   1 &   0 &   0 &   1 &  2\\
			0 &   1 &   0 &  -1 &   0 & -3
		\rowops
			\add[-1]{2}{0}
			\add[-1]{2}{3}
	\end{gmatrix}\\
	\shortintertext{
		Row 1 can not be subtracted from row 3 as it would result in a negative \(b_3\).
		We can, however, subtract row 3 from row 1.
		We obtain a matrix where all coefficients in the last row are negative.
		Thus, we have obtained an optimal solution and \(-z\) is in the last cell of the matrix.
	}
	\implies &\begin{gmatrix}[v]
		  0 &   0 &   1 &   1 &  -1 &  2\\
		  1 &   0 &   0 &   1 &   0 &  3\\
			0 &   1 &   0 &   0 &   1 &  2\\
			0 &   0 &   0 &  -1 &  -1 & -5
	\end{gmatrix}
\end{align}

Thus, we obtain:
\begin{align}
	\vec{x} &= \cvec{
		3\\
		2\\
		2\\
		0\\
		0
	}
\end{align}
This solution indeed satisfies all constraints.
After the \textsc{Gauss-Jordan} transformation,
we have obtained the values for \(t_{k,j}\).
From the structure of \(\hat{A}\) we see that
\begin{align}
	\left(\hat{A}_N\right)_{k,j} &= t_{k,j}
\end{align}
for all \(k \in B\) and \(j \in N\).
What happens with the \(t_{k,j}\) is basically
solving three systems of equations at once.

Generic form of the matrix:
\begin{align}
	\begin{pmatrix}
		a_{11} & a_{12} & a_{13} & \vdots & a_{1n} & b_1\\
		\vdots & \vdots & \vdots & \ddots & \vdots & \vdots\\
		a_{m1} & a_{m2} & a_{m3} & \dotsc & a_{mn} & b_m\\
		c_1 & c_2 & c_3 & \dotsc & c_n & z
	\end{pmatrix}
\end{align}

\paragraph{Example 2:}

\begin{align}
	\max x_2\\
	\shortintertext{such that}
	x_1 - x_2 &\leq 1\\
	-x_1 + x_2 &\leq 2\\
	x_1, x_2 &\geq 0
\end{align}

First of all,
we write this in matrix form.
We also introduce slack variables:
\begin{align}
	&\begin{gmatrix}[v]
		1 & -1 & 1 & 0 & 1\\
		-1 & 1 & 0 & 1 & 2\\
		1 & 0 & 0 & 0 & 0
		\rowops
			\add[1]{0}{1}
			\add[-1]{0}{2}
	\end{gmatrix}\\
	\implies & \begin{gmatrix}[v]
		1 & \mathbf{-1} & 1 & 0 & 1\\
		0 & 0 & 1 & 1 & 3\\
		0 & \mathbf{1} & -1 & 0 & -1
	\end{gmatrix}
\end{align}

We can see in the second column that \(c_2\) is positive and all other values negative or 0.
This means that the solution is unbounded!
Thus, the solution is infinitely high.

Example 3 contains errors!

\section{Duality -- Motivation}\index{duality}

Consider the LOP:
\begin{align}
	\max f\left(\vec{x}\right) &= 2x_1 + 3x_2\\
	\shortintertext{such that}
	4x_1 + 3x_1 &\leq 12 && (\mathrm{I})\\
	2x_1 + x_2 &\leq 3 && (\mathrm{II})\\
	3x_1 + 2x_2 &\leq 4 && (\mathrm{III})\\
	x_1, x_2 &\geq 0
\end{align}
While this system of equation has more constraints than variables,
we get more variables than constraints as soon as we add slack variables.

As \(x_1, x_2 \geq 0\), we have:
\begin{align}
	2x_1 + 3x_1 &\leq 4x_1 + 8x_2 \leq 12
\end{align}

Thus, \(12\) is an upper bound for \(f\left(\vec{x}\right)\).
If we divide the first equality by \(2\),
we obtain an even better bound
\begin{align}
	2x_1 + 3x_2 &\leq 2x_1 + 4x_2 \leq 6\text{.}
\end{align}
Even better, if we calculate \(\frac{1}{3} \cdot (\mathrm{I}) \cdot (\mathrm{II})\).
\begin{align}
	2x_1 + 3x_2 &\leq \frac{1}{3} \left(4x_1 + 8x_2 + 2x_1 + x_2\right)\\
	&\leq \frac{1}{3}\left(12 + 3\right)\\
	&\quad= 5
\end{align}

Hence, \(f\left(\vec{x}\right)\) can not get larger than \(5\).
How good can an upper bound get this way?
So, we are trying to derive an inequality of the form
\begin{align}
	d_1 x_1 + d_2 x_2 &\leq h
\end{align}
with \(d_1 \geq 2\), \(d_2 \geq 3\) and \(h\) as small as possible.
So \(\forall x_1, x_2 \geq 0\) we have
\begin{align}
	2x_1 + 3x_2 &\leq d_1x_1 + d_2x_2 \leq h\text{.}
\end{align}
Combining the 3 inequalities of the original LOP with some non-negative coefficients
\(y_1, y_2, y_3\)
(so that direction of inequalities is not reversed), we obtain
\begin{align}
	\underbrace{\left(4y_1 + 2y_2 + 3y_3\right)}_{d_1} x_1 +
	\underbrace{\left(8y_1 + y_2 + 2y_3\right)}_{d_2} x_2
	&\leq \underbrace{12y_1 + 3y_2 + 4y_3}_{h}
\end{align}

How to choose \(y_i\)?
We need to ensure that \(d_1 \geq 2\) and \(d_2 \geq 3\) and
we want to have \(h\) as small as possible under these constraints.
So we have a new LOP:
\begin{align}
	\min 12y_1 + 3y_2 + 4y_3&\\
	\shortintertext{such that}
	4y_1 + 2y_2 + 3y_3 &\geq 2\\
	8y_1 + y_2 + 2y_3 &\geq 3\\
	y_1, y_2, y_3 &\geq 0
\end{align}
This is called the \cdefname{dual LOP}\index{dual}
to the original LOP.

We now show that \emph{every} feasible solution we can find for the dual LOP
is an upper bound for the original LOP and
a feasible solution to the original LOP is a lower bound for the dual LOP.
The original problem is referred to as the \cdefname{primal problem}\index{primal}.

\begin{align}
	\max \vec{c}^{\top} \vec{x} \text{ s.\,t. } A\vec{x} &\leq \vec{b}, \vec{x} \geq \vec{o} (P)\\
	\min \vec{b}^{\top} \vec{y} \text{ s.\,t. } A^{\top}\vec{y} &\geq \vec{c}, \vec{y} \geq \vec{o} (D)
\end{align}

We can see that \(\vec{y}\) in the dual problem is multiplied with the \emph{transposed}
matrix.
Thus, if the primal problems has 2 variables and 3 constraints,
the dual problem has 3 variables, but only 2 constraints.

We later show that the optimal solution for both problems is the same
and any feasible solution is a bound for the optimal solution.
If the primal optimization problem is unbounded,
the dual problem has no solution.
Vice versa, if the primal problem has no solution,
the dual problem is unbounded.

\section{Duality}

Note that we now calculate \(y^{\top}A\) in the slide.
Let's recall:

Let \(A\) be an \(m \times n\) matrix,
\(\vec{x}\) be an \(m \times 1\) vector,
\(\vec{b}\) be an \(m \times 1\) vector.

\begin{align}
	A \cdot \vec{x} &= \vec{b}\\
	\iff \left(A \cdot x\right)^{\top} &= \vec{b}^{\top}\\
	\iff x^{\top} \cdot A^{\top} &= \vec{b}^{\top}
\end{align}
Thus, we can remove the parentheses for the transposition
by applying the transposition for all inner parts of the parentheses.

We can multiply:
\begin{itemize}
	\item a row vector and a matrix:
		\(\left(1 \times m\right) \cdot \left(m, n\right) = \left(1 \times n\right)\)
	\item a matrix and a column vector:
		\(\left(m \times n\right) \cdot \left(n \times 1\right) = \left(m \times 1\right)\)
\end{itemize}

\section{Duality (7)}

(\(\hat{P}\)) expands upon (\(P\)) by adding slack variables:
\begin{align}
	\max \vec{c}^{\top} \vec{x}&\\
	\shortintertext{s.\,t.}
	A\vec{x} + \vec{z} &= \vec{b}\\
	\iff&\\
	\max \left(\vec{x}^{\top}, \vec{o}^{\top}\right) \cvec{\vec{x}\\\vec{z}}\\
	\shortintertext{s.\,t.}
	\left(A\middle|I\right) \underbrace{\cvec{\vec{x}\\\vec{z}}}_{\vec{\hat{x}}} &= \vec{b}
\end{align}

Note that \(\hat{h}_B\) and \(\hat{h}_N\) have nothing to do with \(h\).
\(\vec{\hat{y}}\) has nothing to do with the \(\vec{y}\) from the duality explanation,
but it is rather the \(\vec{y}\) that was the solution of the LOP as in the simplex algorithm.

\section{Duality (8)}

\begin{align}
	\hat{A}_B^{-1}\hat{A}_B \vec{\hat{x}}_B &= \hat{A}_B^{-1} \hat{A}_B \vec{\hat{y}}_B + \hat{A}_B^{-1}\hat{A}_n \vec{\hat{y}}_N\\
	\iff \vec{\hat{x}}_B &= \vec{y}_B + \hat{A}_B^{-1}\hat{A}_N \vec{\hat{y}}_N\\
	\iff \vec{\hat{y}}_B &= \vec{\hat{x}}_B - \hat{A}_B^{-1} \hat{A}_N \vec{\hat{y}}_N
\end{align}
As \(\vec{\hat{x}}\) is a basic solution, \(\vec{\hat{x}}_N = \vec{o}\),
so there is no \(\vec{\hat{x}}_N\) in the equations.

\section{Duality (10)}

\begin{align}
	\vec{u}^{\top}A &\geq \vec{c}^{\top}\\
	\vec{u} &\geq \vec{o}\\
	\vec{u}^{\top} :&= \vec{c}^{\top} \hat{A}_B^{-1}\\
	\vec{\hat{c}}^{\top} \hat{A}_B^{-1} A &\geq \vec{c}^{\top}\\
	\vec{\hat{c}}^{\top} \hat{A}_B^{-1} &\geq \vec{o}^{\top}
\end{align}

\begin{align}
	A\vec{x} &= \vec{b}\\
	\iff \hat{A}_B \vec{\hat{x}}_B &= \vec{\hat{b}}\\
	\iff \hat{A}_B^{-1} \hat{A}_B \vec{\hat{x}}_B &= \hat{A}_B^{-1} \vec{\hat{b}}
\end{align}

\section{Branch and Bound}

Achtung: hier wird jetzt minimiert statt maximiert!

\begin{itemize}
	\item Grundidee: ein schweres Problem mit einem relaxierteren Problem zusammenführen
	\item \(g\left(x\right)\) in der relaxierten Version kann etwas verändert formuliert werden, muss aber \(= f\left(x\right)\) auf \(R\) sein.
	\item Die Lösungen, die uns nicht gefallen, werden schrittweise ausgeschlossen und dann neu gerechnet,
		um schrittweise der richtigen Lösung näher zu kommen.
	\item Branch and Bound:
		\begin{itemize}
			\item allgemeine Algorithmenidee, die darauf beruht, dass eine Menge in zwei Teilmengen partitioniert und zwei Optimierungsprobleme gelöst werden.
			\item Von den Optima der Teilmengen, muss dann lediglich der kleinere der beiden gewählt werden
			\item Man suche sich zunächst eine optimale Lösung für das relaxierte Problem.
				Die Lösung erfüllt dann ggf. nicht die Ganzzahligkeitsbedingung.
				Teile dann das Problem auf in zwei Teilprobleme,
				wobei der nicht ganzzahlige Parameter einmal kleiner und einmal größer
				als der optimale Wert des relaxierten Problems ist.
			\item Bounding: wenn in einem bestimmten Teilbereich die optimale Lösung
				größer als das bisher bekannte Optimum ist, rechne dort gar nicht erst weiter,
				da man dort keine optimale Lösung finden wird.
		\end{itemize}
	\item Bei der Branch-Operation kommen zusätzliche Nebenbedingungen hinzu,
		die weitere Schlupfvariablen einführen und damit die Berechnung erschweren.
		Dafür wird bei diesen Teilproblemen jeweils der Lösungsraum immer kleiner.
\end{itemize}

\section{Cutting Planes for MIP}

Idee bei Branch and Bound war, immer Teile wegzuschneiden.
Idee von Cutting Planes ist jetzt,
einen chirurgischen Schnitt zu erzeugen,
der eine Ecke so wegschneidet,
dass kein Punkt aus \(M\) weggeschnitten wird.
Schritt für Schritt wird dann der Lösungraum immer weiter kleingeschnitten,
bis die Ecken (und damit auch die optimale Lösung) ganzzahlig sind.

Großer Vorteil ist hier, dass sich die Anzahl der zu lösenden Probleme
nicht ständig verdoppelt.

\chapter{Network Design Problems}

\section{Simple Design Problem}

Der Optimierer kann grundsätzlich aus \(\leq y_e\) immer ein \(= y_e\) machen,
da die Kosten \(\xi_e\) immer \(1\) werden
(ansonsten hätte man geringere Kosten).
Damit ist es möglich, \(E\) viele Slack-Variablen im Optimierungsproblem zu vermeiden
und entsprechend die Matrix klein zu halten.
Dadurch lässt sich \(\sum_{d}\sum_{p} \delta_{edp}x_{dp}\) statt \(y_e\) einsetzen.
Am Ende fallen dadurch nur noch \(D\) Nebenbedingungen an.

Durch das ganze Optimierungskram kommt man zu dem Punkt,
dass die Lösung eine Shortest-Path-Lösung sein wird,
wo nur ein Pfad bei rausfällt.
Man kann also genau so gut problemspezifischere Lösungsalgorithmen wie Dijkstra benutzen.

\section{Capacitated Problems}

Das \emph{Pure Allocation Problem} arbeitet ohne Zielfunktion.
Kapazitätsmangel kann durch Einführung einer Variablen \(z\) berechnet werden,
die in den Nebenbedingungen zu jeder Kapazität addiert und
dann als Zielfunktion minimiert wird.
Dabei kann es auch sein, dass auf Kanten \(z\) draufaddiert wird,
die genug Kapazität haben.
Wenn jetzt \(z\) nach Optimierung negativ ist, weiß man,
dass genug Kapazität vorhanden ist.
Umgekehrt zeigt positives \(z\) an,
dass nicht genug Kapazität vorhanden ist.

Wenn es eine gültige Lösung gibt, gibt es höchstens \(D+E\) Flows,
die \(\geq 0\) sind.
Das folgt wieder daraus, dass man höchstens \(D+E\) (Anzahl Zeilen) viele Variablen
auf \(\neq 0\) setzen kann.
Wenn die \(s_e > 0\), gibt es wieder wegen Single-Path-Allocation genau \(D\) viele Flows.
Alternativ lässt sich auch die insgesamt ungenutzte Kapazität maximieren.
Dabei kann man mit \(r_e\) noch angeben, wie wichtig einem ist,
dass bestimmte Links nicht ausgelastet werden.
Schlimmstenfalls kann man hier auf Gleichheit kommen
(d.\,h. der Link ist ausgelastet).
Hier können bspw. Situationen entstehen,
bei denen kleine Links mit wenig Priorität voll ausgelastet werden.
Minimiert man stattdessen die Rate \(r\),
mit der jeder Link ausgelastet wird,
kann man eine solche Überlastung einzelner Links besser vermeiden.

Bei der Optimierung sollte man die Konsequenzen der Linkauslastung beachten.
Gerade stark oder vollständig ausgelastete Links können nicht gut auf
Lastspitzen reagieren, wodurch es zu vermehrtem Paketverlust kommen kann.

\section{Path Diversity}

Bei der Aufteilung von Flüssen muss immer darauf geachtet werden,
dass niemals einzelne Flows aufgeteilt werden.
Bei der Verteilung einens Flusses auf viele Pfade kann also
der maximale Anteil einens Flows zu jedem Fluss mit beachtet werden,
um besser optimieren zu können.
Weitere Betrachtungen drehen sich um Redundanz,
d.\,h. dass Ausfälle einzelner Links trotzdem vom Netz verkraftet werden können.

Für Path Diversity kann man ein \(n_d\) vorgeben,
auf wie viele Pfade ein Fluss mindestens aufgeteilt werden muss,
um Lastverteilung zu erzwingen.
So kann bspw. erzwungen werden, dass mind. 5 Pfade benutzt werden.

\section{Lower Bounded Flows}

Die bisherigen Probleme waren immer lineare Optimierungsprobleme.
Die nächsten Probleme inklusive Lower Bounded Flows beschäftigens sich
mit ganzzahligen Problemen.

Es ist praktisch nicht sinnvoll,
dass jeder kleine Link mitgenutzt wird,
da man so nur die Ausfallwahrscheinlichkeit erhöht,
ohne signifikante Durchsatzverbesserungen zu erzielen.
Stattdessen möchte man möglicherweise fordern,
dass für jeden Demand eine Untergrenze eingehalten wird,
wie groß der transportierte Anteil sein muss.
Die dafür eingeführten Binärvariablen, ob ein Pfad genutzt wird oder nicht,
macht jedoch das Problem ganzzahlig und erfordert Verfahren wie Branch and Bound,
um das Problem zu lösen.
Das heißt jedoch noch nicht, dass das Problem an sich schwierig ist
(sondern vielleicht nur die Lösungsmethode)!

\section{Limited Demand Split}

Betrachte nun den Extremfall von LBF,
bei dem eine Single Path Allocation gefordert wird.
Dieses Problem ist dann NP-vollständig
und entsprechend ist kein Verfahren bekannt,
dieses Problem in polynomieller Zeit zu lösen.

Man kann die Constraints jetzt so ansetzen,
dass man bspw. eine exakte Anzahl Pfade für jeden Demand gewählt werden.
Das schränkt die Optimierung jedoch stark ein.
Gleichermaßen können zu entspannte Constraints dafür sorgen,
dass bspw. nur kleine Anteile über viele Pfade gehen,
während der größte Anteil (praktisch alles) auf einen Pfad geleitet wird.
Alternativ kann man aber die Anzahl Pfade,
über die der Demand aufgeteilt wird,
nach oben begrenzt werden.

\section{Modular Flow Allocation}

Die Linkkapazitäten sind normalerweise in festgelegten Größen verfügbar.
Es ist nicht möglich, nur einen Bruchteil einer solchen Kapazität zu allokieren.
Bspw. werden nicht \(17.3\,Gbps\), sondern \(20\,Gbps\), \(40\,Gbps\), etc. verschickt
(also die Größen, in denen auch Links von Providern angeboten werden).

Für MFA werden die Demands in \emph{demand modules} aufgeteilt,
die alle eine Kapazität \(L_d\) haben.
Die Gesamtkapazität eines Demands ergibt sich dann durch Multiplikation.

\section{Modular Links}

Linkgrößen werden i.\,d.\,R.~nur in festen Größen angeboten.
Entsprechend muss hierfür die Linkdimensionieren angepasst werden,
sodass immer mindestens so viele Module ausgewählt werden,
dass deren Gesamtkapazität ausreichend für die Linklast ist.

Wenn man jetzt dabei noch die Kosten minimieren möchte,
muss das Optimierungsproblem umformuliert werden,
sodass \(y_e\) nur noch ganzzahlig ist.
Entsprechend ist das Problem nur schwer exakt zu lösen.
Es bietet sich also hier wieder intuitiv an, Heuristiken zu benutzen.
Allerdings kann dies schnell dazu führen, das Optimum weit zu verfehlen.
Es stellt sich heraus, dass das Problem NP-vollständig ist.

Das Problem lässt sich zudem noch abwandeln,
indem Linkmodule verschiedene Linkkapazitäten können
(bspw. in der Praxis bei Glasfasern mit WDM realisierbar).
Dies wird im Problem LMMS modelliert.

\section{Convex Cost and Delay Functions}

Konvexe Kostenfunktionen können angenähert werden,
indem eine lineare Approximation der konvexen Kostenfunktion
als Eingabe für den Optimierer benutzt wird.
Als Approximation können bspw. Teilintervalle jeweils
durch unterschiedliche lineare Funktionen approximiert werden,
wie im Beispiel gezeigt wird.
Dies funktioniert natürlich nur in einem gewissen Intervall;
außerhalb des Intervalls wächst die Originalfunktion dann
deutlich schneller als die lineare Approximation.
In diesem Bereich funktioniert die Approximation dann nicht mehr!

In der auf (5) gezeigten Approximation ist der Funktionswert
für jedes Teilstück der Approximation dann das richtige Geradenstück,
wenn der Wert maximal ist.
Somit lässt sich für die Minimierung die passende Approximation auswählen
und der Zielfunktionswert passt dann auch.
Da der Optimierer immer Eckpunkte des Lösungsraums als optimale Lösungen findet,
ist die approximierte Lösung an diesen Punkten dann auch eine optimale Lösung.
Dadurch ist jedoch trotzdem nicht bekannt,
wie weit der optimale Wert der Approximation
vom tatsächlichen Optimum der Originalfunktion entfernt ist.
In der Praxis zeigt die feingranulare Wahl von \(s_k\) gute Ergebnisse.

Diese Problem lassen sich jetzt auch wieder auf mehrere Links ausweiten.

Manchmal ist auch nur die Steigung interessant.
Hierbei kann man sich die Konvexität zunutze machen,
da jetzt bekannt ist, dass die Steigungen
in aufsteigender Reihenfolge immer mit \(<\) geordnet sind
(d.\,h. \(a_K\) ist das größte \(a_k\) für \(k \in \left\{1, \dotsc, K\right\}\)).
Es bietet sich also an, jedes \(z_i\) größtmöglich zu wählen,
da man sonst mit den nachfolgenden \(z_i\) in Bereichen größerer Kostenanstiege liegt.

\section{Concave Link Dimensioning Functions}

Konkave Funktionen haben im Gegensatz zu konvexen Funktionen immer weiter sinkenden Anstieg.
In unseren Modellen wird zusätzlich noch gefordert,
dass der Anstieg positiv bleibt.
Solche Kostenfunktionen können bspw. bei Hardwarekosten entstehen
(d.\,h. bessere Hardware hat einen kleineren Aufpreis).

Hier bestehen ähnliche Probleme wie für konvexe Funktionen.
Zusätzlich gibt es hier jedoch auch das Problem,
dass es viele lokale Minima gibt.
Wir können dennoch eine ähnliche Strategie wie für konvexe Funktionen benutzen.
Im Gegensatz zu den konvexen Funktionen
können die Probleme für konkave Funktionen nicht als reine LOPs gelöst werden,
sondern benötigen Ganzzahl-Constraints.

Eben ließ sich der „richtige“ Bereich für die \(z_k\) noch recht einfach bestimmen.
Das ist hier jedoch nicht möglich.
Dafür wird jetzt in den Constraints gefordert,
dass \(u_k\) immer nur einmal \(= 1\) sein darf.
Zusätzlich werden noch Hilfsvariablen \(y_k\) eingeführt,
die aufsummiert das \(y\) ergeben und in ihrem Wert durch \(\Delta u_k\)
nach oben beschränkt sind.

Das führt dazu, dass genau ein \(y_k\) ausgewählt wird,
welches dann auch den kleinsten Wert hat.
Alle anderen \(y_k\) bleiben \(=0\).

\section{Budget Constraints}

Kernidee: mache so viel Durchsatz wie möglich, ohne ein Budget zu sprengen.
Es ist dabei insbesondere möglich, mehr als vom Kunden gefordert bereitzustellen.

\section{Incremental Network Design Problems}

Kernidee: Gegeben ein Netz mit bestehenden Kapazitäten,
wie kann ich mein Netz so umbauen,
dass mit möglichst wenig Aufwand zusätzliche Kapazitäten/Demands bereitgestellt/erfüllt
werden können.
Die vorhandenen Kapazitäten sollen also weiter benutzt werden,
aber neue Kapazitäten sollen hinzukommen.

Dabei zeigt sich, dass die Bestandskapazitäten \(c_e\) konstant sind
und damit keinen Einfluss auf die Variablenbelegung haben.
Diese können also theoretisch für die Berechnung der Zielfunktion ignoriert werden.
Dadurch ergibt sich eine Reduktion der Freiheitsgrade und damit auch
eine verschlechterung der optimalen Lösung.

\section{Representing Nodes}

Bisher wurden nur Kantenkapazitäten betrachtet.
Knoten wurden als kostenlos und fehlerfrei angesehen,
was jedoch in der Realität nicht der Fall ist.
Knotenkapazitäten und -kosten können repräsentiert werden,
indem aus jedem Knoten je ein Knotenpaar mit einer verbindenden Kante erzeugt wird.
Alle eingehenden Kanten gehen auf dem hinteren Knoten ein,
alle ausgehenden Kanten gehen vom ausgehenden Knoten aus.
Dann können Kosten des Knotens einfach
als Kantenkosten der verbindenden Kante zwischen den beiden Knoten modelliert werden.

Für ungerichtete Graphen hingegen werden künstlich Eigenkanten eingeführt,
die vom Knoten zu sich selbst gehen und auf der die Kosten des Knotens modelliert werden.
Diese künstliche Kante lässt sich dann wieder in Pfade
und somit auch das Optimierungsproblem einfügen.

\chapter{Network Resilience}

\section{Depth First Search}

\begin{itemize}
	\item Präorder: \(-1\) ist default für alle Knoten, zeigt an, dass der Knoten noch unmarkiert ist.
		Der Wert zählt, der wievielte Knoten das ist, zu dem hingegangen worden ist,
		aka. wann wird welcher Knoten betreten.
	\item Postorder: Wird nach der Rekursion gesetzt.
		Der zählt, der wievielte Knoten das ist, zu dem zurückgekehrt worden ist,
		aka. wann wird welcher Knoten verlassen.
\end{itemize}

Postorder wird hier nicht weiter benötigt, sondern nur Präorder.

Später bei der Klassifikation werden teilweise wieder alle Kanten betrachtet!
Eine Baumkante gehört zum Tiefensuchbaum.
Eine Rückwärtskante geht von einem Knoten zu einem anderen Knoten im Baum,
der im Tiefensuchbaum näher an der Wurzel und im gleichen Zweig ist.
Alles andere sind Kreuzkanten.
Letztere können in einem Tiefensuchbaum nicht auftreten.

\section{Verifying Descendant Relationships via Preordering}

Damit lässt sich prüfen, ob wir noch Netz über einen Großvaterknoten bekommen können,
wenn ein Vaterknoten stirbt.
Somit lässt sich also ermitteln, welche Knoten Artikulationsknoten sind.

Artikulationsknoten sind genau die \(v\), die keine Blätter sind,
und für irgendeinen Unterbaum von \(v\) kein Kindknoten von \(v\)
eine Rückwärtskante zu einem echten Vorfahren von \(v\) hat.
Es gibt dann also Kindknoten von \(v\),
deren Rückwärtskanten höchstens zu \(v\),
jedoch nicht weiter Richtung Wurzel gehen.

\section{Low Value of Nodes}

\(low\left(v\right)\) lässt sich durch den Präorder-Wert von \(v\)
sowie den \(low\)-Werten der \emph{direkten} Kindknoten sowie
einiger ihrer Präorder-Werte zu berechnen.

Der Wert gibt den Präorder-Wert des kleinsten Knotens an,
zu dem man von dem Knoten hin über Rückwärtskanten zurückgehen kann.
\(low\) lässt sich während der Durchführung der Tiefensuche rekursiv berechnen.

Ein Knoten \(v\) (der nicht die Wurzel ist) ist genau dann Artikulationsknoten,
wenn für irgendein Kind der \(low\)-Wert größer oder gleich \(v.pre\) ist.
Sonst würde es einen Pfad von einem Kind zu einem Knoten oberhalb von \(v\) geben.
Wurzelknoten sind genau dann Artikulationsknoten, wenn sie mehr als einen Kindknoten haben,
da genau dann der Weg über die Wurzel der einzige Weg von einem direkten Kind zu einem anderen ist (sonst hätte die DFS das eine direkte Kind als direktes Kind des anderen direkten Kindes gewählt -- gleiche Argumentation wie für Kreuzkanten).
Auch der Test, ob ein Knoten Artikulationsknoten ist,
lässt sich in die Tiefensuche (direkt nach der Berechnung von \(low\)) einbauen.

\vorlesungfoot
\end{document}
