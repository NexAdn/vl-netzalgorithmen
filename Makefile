LTX = latexmk -xelatex -file-line-error -interaction=nonstopmode -halt-on-error

all: vl.pdf zsm.pdf

%.pdf: %.tex $(wildcard zsm.d/*.tex)
	$(LTX) $<
